#pragma once

#include "glm/glm.hpp"
#include "camera.h"

class OrbitCamera : public Camera {
    private:
        glm::vec3 target = glm::vec3(0);
        void process_input(float delta);
    public:
        OrbitCamera(glm::vec3 p_pos);
        OrbitCamera();
        ~OrbitCamera();
        void update(float delta);
        void notify(const std::string &event_name);
        glm::mat4 get_view_matrix() const;
        glm::vec3 get_position() const;
        glm::vec3 get_view_direction() const;
        float get_fov() const;
        void set_fov(float p_fov);
};