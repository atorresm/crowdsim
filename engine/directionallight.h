#pragma once

#include "light.h"
#include "glm/glm.hpp"

class DirectionalLight : public Light {
    private:
        glm::vec3 direction;
    public:
        DirectionalLight(glm::vec3 p_col, glm::vec3 p_dir);
        ~DirectionalLight();
        glm::vec3 get_color() const;
        void set_color(glm::vec3 p_col);
        glm::vec3 get_direction() const;
        void set_direction(glm::vec3 p_dir);
};