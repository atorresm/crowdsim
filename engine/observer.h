#pragma once
#include <string>

class Observer {
    public:
        virtual void notify(const std::string &event_name) = 0;
};