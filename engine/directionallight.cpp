#include "directionallight.h"


DirectionalLight::DirectionalLight(glm::vec3 p_col, glm::vec3 p_dir) {
    this->color = p_col;
    this->direction = p_dir;
}

DirectionalLight::~DirectionalLight() {
    
}

glm::vec3 DirectionalLight::get_color() const {
    return this->color;
}

void DirectionalLight::set_color(glm::vec3 p_col) {
    this->color = p_col;
}

glm::vec3 DirectionalLight::get_direction() const {
    return this->direction;
}

void DirectionalLight::set_direction(glm::vec3 p_dir) {
    this->direction = p_dir;
}
