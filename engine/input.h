#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "glm/glm.hpp"
#include "window.h"
#include "observer.h"

class Input {
    friend class Engine;
    private:
        static double mouse_scroll_y;
        static double mouse_scroll_x;
        static double last_mouse_pos_x;
        static double last_mouse_pos_y;
        static double mouse_pos_x;
        static double mouse_pos_y;
        static GLFWwindow *window;
        static int enum_key_to_glfw_key(int key);
        static void scroll_callback(GLFWwindow* p_window, double xoffset, double yoffset);
        static void update();
        static std::vector<Observer*> scroll_observers; // observers notified when mouse scroll updates
        Input();
    public:
        enum Keys {
            KEY_W,
            KEY_A,
            KEY_S,
            KEY_D,
            KEY_ESC,
        };
        static void init(Window *p_window);
        static bool is_key_pressed(int key);
        static glm::vec2 get_mouse_cursor_position();
        static glm::vec2 get_mouse_position_delta();
        static bool is_left_mouse_button_pressed();
        static bool is_right_mouse_button_pressed();
        static float get_mouse_scroll();
        static void add_scroll_observer(Observer *p_obs);
        ~Input();
};