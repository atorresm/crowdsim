#include "material.h"
#include "texture.h"

Material::Material(const std::string &p_diffuse_tex_path, const std::string &p_specular_tex_path, float p_metallic) {
    this->diffuse_texture = new Texture(p_diffuse_tex_path);
    this->specular_texture = new Texture(p_specular_tex_path);
    this->metallic = p_metallic;
}

Material::Material(Texture *p_diff_tex, Texture *p_spec_tex, float p_metallic) {
    this->diffuse_texture = p_diff_tex;
    this->specular_texture = p_spec_tex;
    this->metallic = p_metallic;
}

Material::Material(float p_metallic) {
    // Using neutral gray texture as default textures
    unsigned char texdata[4] = {180, 180, 180, 255};
    Texture *t = new Texture(texdata, 1, 1, 4);
    this->diffuse_texture = t;
    this->specular_texture = t;
    this->metallic = p_metallic;
}

Material::~Material() {
    if (this->diffuse_texture != nullptr) {
        delete this->diffuse_texture;
    }
    if (this->specular_texture != nullptr) {
        delete this->specular_texture;
    }
}

unsigned int Material::get_diffuse_id() {
    return this->diffuse_texture->get_id();
}

unsigned int Material::get_specular_id() {
    return this->diffuse_texture->get_id();
}

void Material::set_metallic(float p_met) {
    this->metallic = p_met;
}

float Material::get_metallic() {
    return this->metallic;
}
