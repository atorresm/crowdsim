#pragma once

#include <string>

class Texture {
    private:
        unsigned int id;
    public:
        Texture(const std::string &p_tex_path);
        Texture(unsigned char *p_img_data, int p_w, int p_h, int p_channels);
        ~Texture();
        unsigned int get_id() const;
};