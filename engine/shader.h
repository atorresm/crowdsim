#pragma once

#include <iostream>
#include <string>
#include <shaderc/shaderc.hpp>

/// TODO: Add ShaderProgram class, containing both vertex and fragment shader
// this will prevent OpenGL from compiling the same shader more than once
// (currently it gets compiled in Model constructor, this class should have
// a member variable containing the shader program ID for easy sharing between models)
class Shader {
    public:
        enum class ShaderType {
            VERTEX,
            FRAGMENT,
            COMPUTE,
            GEOMETRY,
        };
        Shader();
        Shader(const std::string &glsl_file_path);
        static bool compile_glsl_file_to_spv_file(ShaderType shader_type,
                                const std::string &glsl_file_path, const std::string &output_file_path,
                                bool optimize);
        std::string& get_source_code();
    private:
        std::string source_code;
        static std::string const preprocess_shader(const std::string &shader_id, ShaderType shader_type, 
                                const std::string &p_source_code);
        static shaderc_shader_kind const get_shader_type_from_enum(ShaderType type);
};