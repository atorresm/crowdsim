#pragma once

#include "glm/glm.hpp"

class Environment {
    private:
        glm::vec3 ambient_color;
        glm::vec3 background_color;
    public:
        Environment();
        Environment(glm::vec3 p_ambient_col);
        Environment(glm::vec3 p_ambient_col, glm::vec3 p_background_color);
        ~Environment();
        glm::vec3 get_ambient_color() const;
        void set_ambient_color(glm::vec3 p_col);
        glm::vec3 get_background_color() const;
        void set_background_color(glm::vec3 p_col);
};
