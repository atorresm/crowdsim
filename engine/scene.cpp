#include "scene.h"
#include "engine.h"
#include "sceneuniform.h"
#include "glm/gtc/matrix_transform.hpp"


Scene::Scene(Camera *p_cam, Environment *p_env) {
    this->camera = p_cam;
    this->environment = p_env;
}

Scene::Scene(Camera *p_cam) {
    this->camera = p_cam;
}

Scene::~Scene() {
    
}

void Scene::draw(float p_delta) {
    this->camera->update(p_delta);
    glm::mat4 proj_matrix = glm::perspective(glm::radians(this->camera->get_fov()), (float) Engine::WINDOW_WIDTH / (float) Engine::WINDOW_HEIGHT, 0.1f, 300.0f);
    glm::mat4 view_matrix = this->camera->get_view_matrix();
    SceneUniform scn_info {
        this->lights,
        this->camera->get_position(),
        this->environment->get_ambient_color()
    };
    for (size_t i = 0; i < this->items.size(); i++) {
        this->items[i]->set_projection_matrix(proj_matrix);
        this->items[i]->set_view_matrix(view_matrix);
        this->items[i]->set_scene_info(scn_info);
        this->items[i]->draw(p_delta);
    }
}

void Scene::set_items(std::vector<Model*> p_items) {
    this->items = p_items;
}

std::vector<Model*> Scene::get_items() const {
    return this->items;
}

void Scene::set_camera(Camera *p_cam) {
    this->camera = p_cam;
}

Camera *Scene::get_camera() {
    return this->camera;
}

void Scene::set_lights(std::vector<Light*> p_lights) {
    this->lights = p_lights;
}

std::vector<Light*> Scene::get_lights() const {
    return this->lights;
}

void Scene::set_environment(Environment *p_env) {
    this->environment = p_env;
}

Environment *Scene::get_environment() {
    return this->environment;
}

void Scene::add_item(Model *p_item) {
    this->items.push_back(p_item);
}

void Scene::add_light(Light *p_light) {
    this->lights.push_back(p_light);
}
