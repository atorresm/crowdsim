#pragma once

#include <vector>
#include "glm/glm.hpp"
#include "shader.h"
#include "drawable.h"
#include "light.h"
#include "sceneuniform.h"
#include "material.h"

class Model : public Drawable {
    friend class Scene;
    protected:
        std::vector<float> vertices;
        std::vector<unsigned int> indices;
        glm::vec3 ambient_color;
        Shader vertex_shader;
        Shader fragment_shader;
        unsigned int shader_program;
        unsigned int vao;
        unsigned int vbo;
        unsigned int ebo;
        glm::mat4 transform = glm::mat4(1.0); // model matrix
        glm::vec3 position = glm::vec4(0.0);
        glm::mat4 projection;
        glm::mat4 view = glm::mat4(1.0);
        glm::vec3 scale_vec;
        SceneUniform scene_info;
        Material *material;
        void prepare();
        void prepare_shaders();
        void update_uniforms();
        void set_scene_info(SceneUniform p_info);
    public:
        Model() {};
        Model(const std::vector<float> &p_vertices, Shader p_vertex_shader, Shader p_fragment_shader);
        Model(const std::vector<float> &p_vertices, const std::vector<unsigned int> &p_indices, Shader p_vertex_shader, Shader p_fragment_shader);
        ~Model();
        void draw(float p_delta);
        glm::mat4 get_transform() const;
        glm::mat3 get_scale() const;
        void set_transform(glm::mat4 p_transform);
        void scale(glm::vec3 p_scale);
        glm::mat4 get_projection() const;
        void set_projection_matrix(glm::mat4 p_proj);
        glm::mat4 get_view_matrix() const;
        void set_view_matrix(glm::mat4 p_view);
        glm::vec3 get_position() const;
        void set_position(glm::vec3 p_pos);
        Material *get_material() const;
        void set_material(Material *p_material);
        void rotate_x(float p_degrees);
        void rotate_y(float p_degrees);
        void rotate_z(float p_degrees);
};