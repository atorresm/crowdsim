#include "input.h"
#include <iostream>

// Init static member variables
GLFWwindow *Input::window = nullptr;
double Input::mouse_scroll_y = 0.0;
double Input::mouse_scroll_x = 0.0;
double Input::last_mouse_pos_x = 0.0;
double Input::last_mouse_pos_y = 0.0;
double Input::mouse_pos_x = 0.0;
double Input::mouse_pos_y = 0.0;
std::vector<Observer*> Input::scroll_observers = {};


int Input::enum_key_to_glfw_key(int key) {
    switch (key) {
    case KEY_W:
        return GLFW_KEY_W;
    case KEY_A:
        return GLFW_KEY_A;
    case KEY_S:
        return GLFW_KEY_S;
    case KEY_D:
        return GLFW_KEY_D;
    case KEY_ESC:
        return GLFW_KEY_ESCAPE;
    default:
        std::cout << "Key with keycode " << key << " not defined." << std::endl;
        return -1;
    }
}

Input::Input() {
    
}

void Input::init(Window *p_window) {
    // Get GLFWwindow internal pointer (Window is a friend class of Input)
    window = p_window->window;
    glfwSetScrollCallback(window, scroll_callback);   
}

void Input::scroll_callback(GLFWwindow* p_window, double xoffset, double yoffset) {
    mouse_scroll_x = xoffset;
    mouse_scroll_y = yoffset;
    for (size_t i = 0; i < scroll_observers.size(); i++) {
        scroll_observers[i]->notify("scroll_changed");
    }
}

float Input::get_mouse_scroll() {
    return mouse_scroll_y;
}

bool Input::is_key_pressed(int key) {
    return glfwGetKey(window, enum_key_to_glfw_key(key)) == GLFW_PRESS;
}

glm::vec2 Input::get_mouse_cursor_position() {
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    return glm::vec2(x, y);
}

glm::vec2 Input::get_mouse_position_delta() {
    return glm::vec2(mouse_pos_x - last_mouse_pos_x, mouse_pos_y - last_mouse_pos_y);
}

bool Input::is_left_mouse_button_pressed() {
    return glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
}

bool Input::is_right_mouse_button_pressed() {
    return glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS;
}

void Input::update() {
    last_mouse_pos_x = mouse_pos_x;
    last_mouse_pos_y = mouse_pos_y;
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    mouse_pos_x = x;
    mouse_pos_y = y;
}

void Input::add_scroll_observer(Observer *p_obs) {
    scroll_observers.push_back(p_obs);
}

Input::~Input() {
    
}
