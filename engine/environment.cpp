#include "environment.h"
#include "engine.h"


Environment::Environment() {
    
}

Environment::Environment(glm::vec3 p_ambient_col, glm::vec3 p_background_color) {
    this->ambient_color = p_ambient_col;
    this->set_background_color(p_background_color);
}

Environment::Environment(glm::vec3 p_ambient_col) {
    this->ambient_color = p_ambient_col;
    this->set_background_color(glm::vec3(0.0));
}

Environment::~Environment() {
    
}

glm::vec3 Environment::get_ambient_color() const {
    return this->ambient_color;
}

void Environment::set_ambient_color(glm::vec3 p_col) {
    this->ambient_color = p_col;
}

glm::vec3 Environment::get_background_color() const {
    return this->background_color;
}

void Environment::set_background_color(glm::vec3 p_col) {
    this->background_color = p_col;
    Engine::set_clear_color(this->background_color);
}

