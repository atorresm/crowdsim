#include "window.h"
#include <iostream>

Window::Window(int p_w, int p_h) {
    if (p_w < 0 || p_h < 0) {
        this->correctly_initialized = false;
        std::cout << "Width and height must be higher than 0" << std::endl;
        return;
    }
    
    this->w = p_w;
    this->h = p_h;
    
    // Initialize OpenGL
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create window
    GLFWwindow *win = glfwCreateWindow(this->w, this->h, "Crowd simulation", nullptr, nullptr);
    if (win == nullptr) {
        this->correctly_initialized = false;
        std::cout << "Couldn't create GLFW window" << std::endl;
        glfwTerminate();
        return;
    }
    this->window = win;
    glfwMakeContextCurrent(this->window);
    glfwSetFramebufferSizeCallback(this->window, this->viewport_size_callback);

    // Load OpenGL function pointers
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        this->correctly_initialized = false;
        std::cout << "Couldn't initialize GLAD" << std::endl;
        glfwTerminate();
        return;
    }
    glEnable(GL_DEPTH_TEST);
    this->set_clear_color(0.0, 0.0, 0.0, 1.0);
    this->correctly_initialized = true;
}

Window::~Window() {
    glfwTerminate();
}

int Window::width() {
    return this->w;
}

int Window::height() {
    return this->h;
}

bool Window::should_close() {
    return glfwWindowShouldClose(this->window);
}

bool Window::ok() {
    return this->correctly_initialized;   
}

void Window::update() {
    glfwSwapBuffers(this->window);
    glfwPollEvents();
}

float Window::delta() {
    float current_time = glfwGetTime();
    float dt = current_time - this->last_frame_time;
    this->last_frame_time = current_time;
    return dt;
}

void Window::set_clear_color(float r, float g, float b, float a) {
    glClearColor(r, g, b, a);
}

void Window::clear() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::viewport_size_callback(GLFWwindow *p_window, int p_w, int p_h) {
    glViewport(0, 0, p_w, p_h);
}