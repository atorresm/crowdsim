#pragma once
#include "camera.h"

class FlyCamera : public Camera {
    private:
        glm::vec3 front;
        glm::vec3 up;
        glm::vec3 right;
        float yaw = 0;
        float pitch = 0;
        float movement_speed = 4.5f;
        void process_input(float delta);
    public:
        FlyCamera(glm::vec3 p_pos, float p_fov = 45.f);
        FlyCamera();
        ~FlyCamera();
        void update(float delta);
        glm::mat4 get_view_matrix() const;
        glm::vec3 get_position() const;
        glm::vec3 get_view_direction() const;
        void notify(const std::string &event_name);
        float get_fov() const;
        void set_fov(float p_fov);
};