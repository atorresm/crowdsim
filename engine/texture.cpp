#include "texture.h"
#include "glad/glad.h"
#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
#include "stb_image.h"
#include <iostream>

Texture::Texture(const std::string &p_tex_path) {
    // Texture parameters
    glGenTextures(1, &(this->id));
    glBindTexture(GL_TEXTURE_2D, this->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Load image
    int w, h, channels;
    stbi_set_flip_vertically_on_load(true); // Flip Y-axis to be compatible with OpenGL
    unsigned char *img = stbi_load(p_tex_path.c_str(), &w, &h, &channels, 0);
    if (img) {
        GLenum format;
        switch (channels) {
            case 1:
                format = GL_RED;
                break;
            case 2:
                format = GL_RG;
                break;
            case 3:
                format = GL_RGB;
                break;
            default:
                format = GL_RGBA;
                break;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, format, GL_UNSIGNED_BYTE, img);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        std::cout << "Error: failed to load texture at " << p_tex_path << std::endl;
    }
    stbi_image_free(img);
}

Texture::Texture(unsigned char *p_img_data, int p_w, int p_h, int p_channels) {
    // Texture parameters
    glGenTextures(1, &(this->id));
    glBindTexture(GL_TEXTURE_2D, this->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLenum format;
    switch (p_channels) {
        case 1:
            format = GL_RED;
            break;
        case 2:
            format = GL_RG;
            break;
        case 3:
            format = GL_RGB;
            break;
        default:
            format = GL_RGBA;
            break;
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, p_w, p_h, 0, format, GL_UNSIGNED_BYTE, p_img_data);
    glGenerateMipmap(GL_TEXTURE_2D);
}

unsigned int Texture::get_id() const {
    return this->id;
}

Texture::~Texture() {
    glDeleteTextures(1, &(this->id));
}
