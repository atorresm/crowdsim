#pragma once

#include <vector>
#include "model.h"
#include "camera.h"
#include "light.h"
#include "environment.h"

class Scene : public Drawable {
    private:
        std::vector<Model*> items;
        std::vector<Light*> lights;
        Environment *environment;
        Camera *camera;
    public:
        Scene(Camera *p_cam, Environment *p_env);
        Scene(Camera *p_cam);
        ~Scene();
        void draw(float p_delta);
        void set_items(std::vector<Model*> p_items);
        std::vector<Model*> get_items() const;
        void set_camera(Camera *p_cam);
        Camera *get_camera();
        void set_lights(std::vector<Light*> p_lights);
        std::vector<Light*> get_lights() const;
        void set_environment(Environment *p_env);
        Environment *get_environment();
        void add_item(Model *p_item);
        void add_light(Light *p_light);
};