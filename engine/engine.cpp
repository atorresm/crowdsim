#include "engine.h"
#include "input.h"


// Init static member variables
int Engine::WINDOW_HEIGHT = -1;
int Engine::WINDOW_WIDTH = -1;
Window *Engine::window = nullptr;


Engine::Engine() {
    
}

Engine::~Engine() {
    
}

void Engine::init(int p_window_width, int p_window_height) {
    window = new Window(p_window_width, p_window_height);
    WINDOW_HEIGHT = p_window_height;
    WINDOW_WIDTH = p_window_width;
    Input::init(window);
}

float Engine::delta() {
    return window->delta();
}

void Engine::update() {
    window->update();
    Input::update();
    window->clear();
}

bool Engine::should_close() {
    return window->should_close();
}

void Engine::set_clear_color(glm::vec3 p_col) {
    window->set_clear_color(p_col.r, p_col.g, p_col.b, 1.0);
}
