#pragma once

#include <string>
#include "model.h"
#include "texture.h"

class Sprite : public Model {
    private:
        Texture *texture;
        glm::vec2 position;
        glm::vec2 size;
        float rotation;
    public:
        Sprite();
        ~Sprite();
        Sprite(const std::string &texture_path, glm::vec2 p_size);
        void draw(float p_delta);
        void set_rotation(float p_degrees);
        float get_rotation() const;
        void rotate(float p_degrees);
        void scale(glm::vec2 p_scale);
        void set_position(glm::vec2 p_position);
        glm::vec2 get_position() const;
        glm::vec2 get_size() const;
};
