#pragma once

#include <vector>
#include "light.h"
#include "glm/glm.hpp"

// Struct holding info passed to models from scene
// (lighting, camera pos, ambient light, etc...)
struct SceneUniform {
    std::vector<Light*> lights;
    glm::vec3 camera_position;
    glm::vec3 ambient_color;
};
