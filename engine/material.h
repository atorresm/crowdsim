#pragma once

#include <string>
#include "texture.h"

class Material {
    private:
        Texture *diffuse_texture;
        Texture *specular_texture;
        float metallic;
    public:
        Material();
        Material(const std::string &p_diffuse_tex_path, const std::string &p_specular_tex_path, float p_metallic);
        Material(Texture *p_diff_tex, Texture *p_spec_tex, float p_metallic);
        Material(float p_metallic);
        ~Material();
        unsigned int get_diffuse_id();
        unsigned int get_specular_id();
        void set_metallic(float p_met);
        float get_metallic();
};