#pragma once

class Drawable {
    public:
        virtual void draw(float p_delta) = 0;
};