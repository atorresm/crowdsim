#include "orbitcamera.h"
#include "input.h"
#include "engine.h"
#include "glm/gtc/matrix_transform.hpp"


OrbitCamera::OrbitCamera(glm::vec3 p_pos) {
    this->position = p_pos;
    Input::add_scroll_observer(this);
}

OrbitCamera::OrbitCamera() {
    this->position = glm::vec3(5.0);
    Input::add_scroll_observer(this);
}

OrbitCamera::~OrbitCamera() {
    
}

void OrbitCamera::process_input(float delta) {
    // Mouse input
    if (Input::is_right_mouse_button_pressed()) {
        glm::vec4 pos = glm::vec4(this->position, 1.0);
        glm::vec4 tar = glm::vec4(this->target, 1.0);

        // Calculate angles
        float angle_x_scale = -(2 * M_PI / Engine::WINDOW_WIDTH);
        float angle_y_scale = -(M_PI / Engine::WINDOW_HEIGHT);
        glm::vec2 mouse_delta = Input::get_mouse_position_delta();
        float dx = mouse_delta.x * angle_x_scale;
        float dy = mouse_delta.y * angle_y_scale;

        glm::mat4 rot_x = glm::rotate(glm::mat4(1.0), dx, glm::vec3(0.0, 1.0, 0.0));
        pos = (rot_x * (pos - tar)) + tar;
        glm::vec4 posx = glm::vec4(pos);

        glm::vec3 right = glm::transpose(this->view)[0];
        glm::mat4 rot_y = glm::rotate(glm::mat4(1.0), dy, right);
        pos = (rot_y * (pos - tar)) + tar;
        
        float cos_angle = glm::dot(glm::vec3(0.0, 1.0, 0.0), glm::normalize(glm::vec3(pos.x, pos.y, pos.z)));
        if (std::abs(cos_angle) > 0.98f) {
            // Revert y-rotation to prevent it from aligning to Y-axis
            this->position = posx;
        } else {
            this->position = pos;
        }
    }
}

void OrbitCamera::notify(const std::string &event_name) {
    // Currently observing only scroll from Input singleton, so update that
    // Zooming
    float zoom = Input::get_mouse_scroll();
    glm::vec3 view_dir = this->get_view_direction();
    glm::vec3 new_pos = this->position + view_dir * zoom * 1.2f;
    if (glm::length(new_pos) > 0.5f) {
        this->position = new_pos;
    }
}

void OrbitCamera::update(float delta) {
    this->process_input(delta);
    this->view = glm::lookAt(this->position, this->target, glm::vec3(0.0, 1.0, 0.0));
}

glm::mat4 OrbitCamera::get_view_matrix() const {
    return this->view;
}

glm::vec3 OrbitCamera::get_position() const {
    return this->position;
}

glm::vec3 OrbitCamera::get_view_direction() const {
    return -glm::transpose(this->view)[2];
}

float OrbitCamera::get_fov() const {
    return this->fov;
}

void OrbitCamera::set_fov(float p_fov) {
    this->fov = p_fov;
}

