#pragma once

#include "observer.h"
#include "glm/glm.hpp"

class Camera : public Observer {
    protected:
        glm::mat4 view = glm::mat4(0);
        glm::vec3 position = glm::vec3(0);
        float fov = 45.f;
        virtual void process_input(float delta) = 0;
    public:
        virtual void update(float delta) = 0;
        virtual void notify(const std::string &event_name) = 0;
        virtual glm::mat4 get_view_matrix() const = 0;
        virtual glm::vec3 get_position() const = 0;
        virtual glm::vec3 get_view_direction() const = 0;
        virtual float get_fov() const = 0;
        virtual void set_fov(float p_fov) = 0;
};