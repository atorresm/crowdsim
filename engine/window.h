#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Window {
    friend class Input;
    private:
        int w;
        int h;
        bool correctly_initialized;
        float last_frame_time = 0.0f;
        GLFWwindow *window;
        static void viewport_size_callback(GLFWwindow *p_window, int p_w, int p_h);
    public:
        Window(int p_w, int p_h);
        ~Window();
        int width();
        int height();
        bool should_close();
        bool ok();
        void update();
        void set_clear_color(float r, float g, float b, float a);
        void clear();
        float delta();
};