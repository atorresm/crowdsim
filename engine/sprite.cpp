#include <iostream>
#include <cmath>
#include "glad/glad.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "texture.h"
#include "sprite.h"

Sprite::Sprite() {
    
}

Sprite::Sprite(const std::string &texture_path, glm::vec2 p_size) {
    this->vertices = {
         // positions         // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
    };
    this->indices = {
        0, 1, 3,
        1, 2, 3
    };
    this->vertex_shader = Shader("res/shaders/sprite_vertex.glsl");
    this->fragment_shader = Shader("res/shaders/sprite_fragment.glsl");
    this->size = p_size;
    this->scale_vec = glm::vec3(this->size, 1.0);
    this->rotation = 0;

    // Build projection matrix
    std::vector<GLint> viewport_size(4);
    glGetIntegerv(GL_VIEWPORT, viewport_size.data());
    int w = viewport_size[2];
    int h = viewport_size[3];
    this->projection = glm::ortho(0.f, (float) w, (float) h, 0.f, -1.f, 1.f);

    this->prepare();
    this->texture = new Texture(texture_path);
}

void Sprite::draw(float p_delta) {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->texture->get_id());
    glUseProgram(this->shader_program);
    glBindVertexArray(this->vao);

    glm::mat4 tform = glm::mat4(1.0);
    tform = glm::translate(tform, glm::vec3(this->position, 0.0f));
    tform = glm::rotate(tform, glm::radians(this->rotation), glm::vec3(0.0, 0.0, 1.0));
    tform = glm::scale(tform, this->scale_vec);
    
    // Pass matrices to shader
    unsigned int loc = glGetUniformLocation(this->shader_program, "transform");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(tform));
    loc = glGetUniformLocation(this->shader_program, "view");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->view));
    loc = glGetUniformLocation(this->shader_program, "projection");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->projection));

    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, nullptr);
}

Sprite::~Sprite() {
    delete this->texture;
}

void Sprite::set_rotation(float p_degrees) {
    this->rotation = p_degrees;
}

float Sprite::get_rotation() const {
    return this->rotation;
}

void Sprite::rotate(float p_degrees) {
    this->rotation = std::fmod(this->rotation + p_degrees, 360);
}

void Sprite::scale(glm::vec2 p_scale) {
    this->scale_vec = glm::vec3(p_scale * this->size, 1.0);
}

void Sprite::set_position(glm::vec2 p_position) {
    this->position = p_position;
}

glm::vec2 Sprite::get_position() const {
    return this->position;
}

glm::vec2 Sprite::get_size() const {
    return this->size;
}