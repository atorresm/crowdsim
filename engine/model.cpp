#include <iostream>
#include "glad/glad.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "model.h"
#include "directionallight.h"
#include "glm/gtx/string_cast.hpp"


Model::Model(const std::vector<float> &p_vertices, Shader p_vertex_shader, Shader p_fragment_shader) {
    this->vertices = p_vertices;
    this->vertex_shader = p_vertex_shader;
    this->fragment_shader = p_fragment_shader;
    this->transform = glm::mat4(1.0);
    // Build initial projection matrix
    std::vector<GLint> viewport_size(4);
    glGetIntegerv(GL_VIEWPORT, viewport_size.data());
    int w = viewport_size[2];
    int h = viewport_size[3];
    this->projection = glm::perspective(glm::radians(50.0f), (float) w / (float) h, 0.1f, 150.f);
    prepare();
};


Model::Model(const std::vector<float> &p_vertices, const std::vector<unsigned int> &p_indices, Shader p_vertex_shader, Shader p_fragment_shader) {
    this->vertices = p_vertices;
    this->indices = p_indices;
    this->vertex_shader = p_vertex_shader;
    this->fragment_shader = p_fragment_shader;
    this->transform = glm::mat4(1.0);

    // Build projection matrix
    std::vector<GLint> viewport_size(4);
    glGetIntegerv(GL_VIEWPORT, viewport_size.data());
    int w = viewport_size[2];
    int h = viewport_size[3];
    this->projection = glm::perspective(glm::radians(50.0f), (float) w / (float) h, 0.1f, 150.f);

    prepare();
};

Model::~Model() {
    glDeleteVertexArrays(1, &(this->vao));
    glDeleteProgram(this->shader_program);
    // Cleanup buffers
    glDeleteBuffers(1, &(this->vbo));
    glDeleteBuffers(1, &(this->ebo));
};

void Model::prepare() {
    glGenVertexArrays(1, &(this->vao));
    glGenBuffers(1, &(this->vbo));
    glGenBuffers(1, &(this->ebo));

    glBindVertexArray(this->vao);
    
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * this->vertices.size(), this->vertices.data(), GL_STATIC_DRAW);

    // If indices were entered, create Element Buffer Object
    if (!this->indices.empty()) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(float) * this->indices.size(), this->indices.data(), GL_STATIC_DRAW);
    }

    // Vertex info:
    // x y z nx ny nz uv.x uv.y (8 floats)
    // position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);
    // normals
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // texture coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) (6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    // Unbind VAO and VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    this->material = new Material(1.0);

    this->prepare_shaders();
};

void Model::prepare_shaders() {
    // Prepare shaders
    int vs = glCreateShader(GL_VERTEX_SHADER);
    int fs = glCreateShader(GL_FRAGMENT_SHADER);
    const char *v_source = this->vertex_shader.get_source_code().c_str();
    const char *f_source = this->fragment_shader.get_source_code().c_str();

    glShaderSource(vs, 1, &v_source, nullptr);
    glShaderSource(fs, 1, &f_source, nullptr);
    
    // Compile shaders
    int res;
    char res_info[512];
    glCompileShader(vs);
    glGetShaderiv(vs, GL_COMPILE_STATUS, &res);
    if (!res) {
        glGetShaderInfoLog(vs, 512, nullptr, res_info);
        std::cout << "Error compiling vertex shader\n" << res_info << std::endl;
    }
    glCompileShader(fs);
    glGetShaderiv(fs, GL_COMPILE_STATUS, &res);
    if (!res) {
        glGetShaderInfoLog(fs, 512, nullptr, res_info);
        std::cout << "Error compiling fragment shader\n" << res_info << std::endl;
    }

    // Create shader program
    this->shader_program = glCreateProgram();
    glAttachShader(this->shader_program, vs);
    glAttachShader(this->shader_program, fs);
    glLinkProgram(this->shader_program);
    glGetProgramiv(this->shader_program, GL_LINK_STATUS, &res);
    if (!res) {
        glGetProgramInfoLog(this->shader_program, 512, nullptr, res_info);
        std::cout << "Error linking shader program\n" << res_info << std::endl;
    }

    glDeleteShader(vs);
    glDeleteShader(fs);
}

void Model::update_uniforms() {
    glUseProgram(this->shader_program);
    glm::mat4 tform = glm::translate(this->transform, this->position);

    // Pass uniforms to shader
    unsigned int loc = glGetUniformLocation(this->shader_program, "model");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(tform));
    loc = glGetUniformLocation(this->shader_program, "view");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->view));
    loc = glGetUniformLocation(this->shader_program, "projection");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(this->projection));

    if (this->scene_info.lights.size() > 0) {
        DirectionalLight *d_light = dynamic_cast<DirectionalLight*>(this->scene_info.lights[0]);
        /// TODO: assuming just one directional light is applied for now
        loc = glGetUniformLocation(this->shader_program, "directional_light.direction");
        glUniform3fv(loc, 1, glm::value_ptr(d_light->get_direction()));
        loc = glGetUniformLocation(this->shader_program, "directional_light.color");
        glUniform3fv(loc, 1, glm::value_ptr(d_light->get_color()));
    }

    loc = glGetUniformLocation(this->shader_program, "ambient_color");
    glUniform3fv(loc, 1, glm::value_ptr(this->scene_info.ambient_color));
    loc = glGetUniformLocation(this->shader_program, "material.metallic");
    glUniform1f(loc, this->material->get_metallic());
    loc = glGetUniformLocation(this->shader_program, "camera_position");
    glUniform3fv(loc, 1, glm::value_ptr(this->scene_info.camera_position));
}

void Model::draw(float p_delta) {
    glBindVertexArray(this->vao);
    
    // Bind material textures
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->material->get_diffuse_id());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, this->material->get_specular_id());
    this->update_uniforms();

    if (!this->indices.empty()) {
        glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, nullptr);
    } else {
        glDrawArrays(GL_TRIANGLES, 0, this->vertices.size() / 3);
    }
};

glm::mat4 Model::get_transform() const {
    return this->transform;
}

void Model::set_transform(glm::mat4 p_transform) {
    this->transform = p_transform;
}

void Model::rotate_x(float p_degrees) {
    this->transform = glm::rotate(this->transform, glm::radians(p_degrees), glm::vec3(1.0, 0.0, 0.0));
}

void Model::rotate_y(float p_degrees) {
    this->transform = glm::rotate(this->transform, glm::radians(p_degrees), glm::vec3(0.0, 1.0, 0.0));
}

void Model::rotate_z(float p_degrees) {
    this->transform = glm::rotate(this->transform, glm::radians(p_degrees), glm::vec3(0.0, 0.0, 1.0));
}

// TODO: Might produce segfault
void Model::scale(glm::vec3 p_scale) {
    this->scale_vec = p_scale;
    this->transform = glm::scale(this->transform, this->scale_vec);
}

glm::mat4 Model::get_projection() const {
    return this->projection;
}

void Model::set_projection_matrix(glm::mat4 p_proj) {
    this->projection = p_proj;
}

glm::mat4 Model::get_view_matrix() const {
    return this->view;
}

void Model::set_view_matrix(glm::mat4 p_view) {
    this->view = p_view;
}

glm::vec3 Model::get_position() const {
    return this->position;
}

void Model::set_position(glm::vec3 p_pos) {
    this->position = p_pos;
}

void Model::set_scene_info(SceneUniform p_info) {
    this->scene_info = p_info;
}

Material *Model::get_material() const {
    return this->material;
}

void Model::set_material(Material *p_material) {
    this->material = p_material;
}
