#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include "shader.h"


Shader::Shader() {

};

Shader::Shader(const std::string &glsl_file_path) {
    std::ifstream file(glsl_file_path);
    if (!file.is_open()) {
        std::cout << "ERROR: Error opening shader file at " << glsl_file_path << std::endl;
        file.close();
        return;
    }
    std::ostringstream sstream;
    sstream << file.rdbuf();
    this->source_code = sstream.str();
    file.close();
};

std::string& Shader::get_source_code() {
    return this->source_code;
};

shaderc_shader_kind const Shader::get_shader_type_from_enum(ShaderType type) {
    switch (type) {
    case ShaderType::VERTEX:
        return shaderc_vertex_shader;
    case ShaderType::FRAGMENT:
        return shaderc_fragment_shader;
    case ShaderType::COMPUTE:
        return shaderc_compute_shader;
    case ShaderType::GEOMETRY:
        return shaderc_geometry_shader;
    default:
        // Default: compute
        return shaderc_compute_shader;
        break;
    }
}

std::string const Shader::preprocess_shader(const std::string &shader_id, ShaderType shader_type,
                                const std::string &p_source_code) {
    
    // Preprocess shader
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;
    shaderc::PreprocessedSourceCompilationResult res = compiler.PreprocessGlsl(p_source_code, get_shader_type_from_enum(shader_type), shader_id.c_str(), options);
    if (res.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cerr << "ERROR: At preprocessing shader: " << res.GetErrorMessage() << std::endl;
        return "";
    }
    return {res.cbegin(), res.cend()};
}

// Adapted from:
// https://github.com/google/shaderc/blob/master/examples/online-compile/main.cc
bool Shader::compile_glsl_file_to_spv_file(ShaderType shader_type,
                                const std::string &glsl_file_path, const std::string &output_file_path,
                                bool optimize) {
    
    // Check file
    std::ifstream file(glsl_file_path);
    if (!file.is_open()) {
        std::cout << "ERROR: Error opening shader file at " << glsl_file_path << std::endl;
        return false;
    }

    // Preprocess shader
    std::ostringstream sstream;
    sstream << file.rdbuf();
    const std::string str(sstream.str());  
    const std::string source = preprocess_shader(glsl_file_path, shader_type, str);
    if (source == "") {
        return false;
    }

    // Compile shader
    shaderc::Compiler compiler;
    shaderc::CompileOptions opts;
    if (optimize) {
        opts.SetOptimizationLevel(shaderc_optimization_level_performance);
    }
    shaderc::SpvCompilationResult res = compiler.CompileGlslToSpv(source, get_shader_type_from_enum(shader_type), glsl_file_path.c_str(), opts);
    if (res.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cout << "ERROR: Error compiling shader: " << res.GetErrorMessage() << std::endl;
        return false;
    }

    // Write spv to binary file
    std::ofstream outfile(output_file_path, std::ios::out | std::ios::binary);
    if (!outfile) {
        std::cout << "ERROR: Error creating/opening file at: " << output_file_path << std::endl;
    }

    // Convert uint32_t to char
    std::vector<uint32_t> spirv = {res.cbegin(), res.cend()};
    for (int i = 0; i < (int) spirv.size(); ++i) {
        uint32_t word = spirv[i];
        outfile.write((const char*) &word, 4);
    }

    if (outfile.bad()) {
        std::cout << "ERROR: Error writing to file " << output_file_path << std::endl;
        outfile.close();
        return false;
    }
    
    outfile.close();

    return true;
}