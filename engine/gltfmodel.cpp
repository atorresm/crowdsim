#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_IMPLEMENTATION
#include "gltfmodel.h"
#include "glad/glad.h"
#include <iostream>


GLTFModel::GLTFModel(const std::string &p_model_path, Shader p_vertex_shader, Shader p_fragment_shader) {
    this->vertex_shader = p_vertex_shader;
    this->fragment_shader = p_fragment_shader;
    this->load_model(p_model_path);
}

GLTFModel::~GLTFModel() {
    delete this->material;
    glDeleteBuffers(this->vbos.size(), vbos.data());
}

void GLTFModel::load_model(const std::string &p_model_path) {
    gltf::TinyGLTF loader;
    std::string err;
    std::string warn;

    bool ok = loader.LoadASCIIFromFile(&(this->model), &err, &warn, p_model_path);
    if (!err.empty()) {
        std::cout << "Error loading GLTF file: " << err << "\n";
    }
    if (!warn.empty()) {
        std::cout << "Warning loading GLTF file: " << warn << "\n";
    }
    if (!ok) {
        std::cout << "Model with filename " << p_model_path << " couldn't be loaded." << "\n";
        return;
    }

    // Initialize VBOs vector
    this->vbos = std::vector<unsigned int>(this->model.bufferViews.size());

    // Bind model to VAO
    glGenVertexArrays(1, &(this->vao));
    glBindVertexArray(this->vao);

    const gltf::Scene &scn = this->model.scenes[this->model.defaultScene];
    for (size_t i = 0; i < scn.nodes.size(); i++) {
        assert((scn.nodes[i] >= 0) && (scn.nodes[i] < model.nodes.size()));
        this->bind_gltf_node(this->model.nodes[scn.nodes[i]]);
    }

    this->prepare_shaders();
    glBindVertexArray(0);
}

void GLTFModel::bind_gltf_node(const gltf::Node &node) {
    if ((node.mesh >= 0) && (node.mesh < this->model.meshes.size())) {
        this->bind_mesh(this->model.meshes[node.mesh]);
    }
    for (size_t i = 0; i < node.children.size(); i++) {
        // Bind child nodes
        assert((node.children[i] >= 0) && (node.children[i] < model.nodes.size()));
        this->bind_gltf_node(this->model.nodes[node.children[i]]);
    }
}

void GLTFModel::bind_mesh(const gltf::Mesh &mesh) {
    for (size_t i = 0; i < this->model.bufferViews.size(); i++) {
        gltf::BufferView &buffer_view = this->model.bufferViews[i];
        gltf::Buffer &buffer = this->model.buffers[buffer_view.buffer];

        // Create VBO with buffer info
        unsigned int vbo_id;
        glGenBuffers(1, &vbo_id);
        this->vbos[i] = vbo_id;
        glBindBuffer(buffer_view.target, vbo_id);
        glBufferData(buffer_view.target, buffer_view.byteLength, &buffer.data.at(0) + buffer_view.byteOffset, GL_STATIC_DRAW);
    }

    for (size_t i = 0; i < mesh.primitives.size(); i++) {
        const auto &attribs = mesh.primitives[i].attributes;
        
        // Attributes
        for (auto &attrib : attribs) {
            gltf::Accessor accesor = this->model.accessors[attrib.second];
            int stride = accesor.ByteStride(this->model.bufferViews[accesor.bufferView]);
            glBindBuffer(GL_ARRAY_BUFFER, this->vbos[accesor.bufferView]);
            
            int size = 1;
            if (accesor.type != TINYGLTF_TYPE_SCALAR) {
                size = accesor.type;
            }

            int att_arr = -1;
            if (attrib.first.compare("POSITION") == 0) {
                att_arr = 0;
            } else if (attrib.first.compare("NORMAL") == 0) {
                att_arr = 1;
            } else if (attrib.first.compare("TEXCOORD_0") == 0) {
                att_arr = 2;
            }
            if (att_arr == -1) {
                std::cout << "Couldn't identify attrib array with name: " << attrib.first << std::endl;
            } else {
                glEnableVertexAttribArray(att_arr);
                glVertexAttribPointer(att_arr, size, accesor.componentType, accesor.normalized, stride, (void*) accesor.byteOffset);
            }
        }

        // Handle textures
        if (this->model.images.size() > 0) {
            /// TODO: assuming textures[0] is diffuse and textures[1] is specular
            gltf::Image &diff = this->model.images[this->model.textures[0].source];
            gltf::Image &spec = this->model.images[this->model.textures[1].source];
            Texture *diff_tex = new Texture(&diff.image.at(0), diff.width, diff.height, diff.component);
            Texture *spec_tex = new Texture(&spec.image.at(0), spec.width, spec.height, spec.component);
            this->material = new Material(diff_tex, spec_tex, 1.0);
        } else {
            this->material = new Material(1.0);
        }
    }
}

void GLTFModel::draw_gltf_node(const gltf::Node &node) {
    // Draw mesh for current node
    if ((node.mesh >= 0) && (node.mesh < this->model.meshes.size())) {
        const gltf::Mesh &mesh = this->model.meshes[node.mesh];
        for (size_t i = 0; i < mesh.primitives.size(); i++) {
            const gltf::Primitive &primitive = mesh.primitives[i];
            const gltf::Accessor &accesor = this->model.accessors[primitive.indices];
            glDrawElements(primitive.mode, accesor.count, accesor.componentType, (void*) accesor.byteOffset);
        }
    }
    // Draw children nodes
    for (size_t i = 0; i < node.children.size(); i++) {
        this->draw_gltf_node(this->model.nodes[node.children[i]]);
    }
}

void GLTFModel::draw(float p_delta) {
    glBindVertexArray(this->vao);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->material->get_diffuse_id());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, this->material->get_specular_id());
    this->update_uniforms();
    
    const tinygltf::Scene &scene = model.scenes[model.defaultScene];
    for (size_t i = 0; i < scene.nodes.size(); i++) {
        this->draw_gltf_node(this->model.nodes[scene.nodes[i]]);
    }

    glBindVertexArray(0);
}
