#include "tinygltf/tiny_gltf.h"
#include "model.h"
#include "shader.h"
#include <string>
#include <vector>

namespace gltf = tinygltf;

class GLTFModel : public Model {
    private:
        gltf::Model model;
        std::vector<unsigned int> vbos;
        void load_model(const std::string &p_model_path);
        // methods for recursively bind nodes and their meshes
        void bind_gltf_node(const gltf::Node &node);
        void bind_mesh(const gltf::Mesh &mesh);
        void draw_gltf_node(const gltf::Node &node);
    public:
        GLTFModel(const std::string &p_model_path, Shader p_vertex_shader, Shader p_fragment_shader);
        ~GLTFModel();
        void draw(float p_delta);
};