#include "flycamera.h"
#include "input.h"
#include "engine.h"
#include "glm/gtc/matrix_transform.hpp"


FlyCamera::FlyCamera() {
    Input::add_scroll_observer(this);
    this->position = glm::vec3(0, 5, 0);
}

FlyCamera::~FlyCamera() {
    
}

void FlyCamera::update(float delta) {
    this->process_input(delta);
    // Update camera vectors
    glm::vec3 new_front;
    new_front.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
    new_front.y = sin(glm::radians(this->pitch));
    new_front.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
    this->front = glm::normalize(new_front);
    this->right = glm::normalize(glm::cross(this->front, glm::vec3(0, 1, 0)));
    this->up = glm::normalize(glm::cross(this->right, this->front));
}

void FlyCamera::process_input(float delta) {
    if (!Input::is_right_mouse_button_pressed()) {
        return;
    }

    float vel = this->movement_speed * delta;
    if (Input::is_key_pressed(Input::KEY_W)) {
        // forward
        this->position += this->front * vel;
    }
    if (Input::is_key_pressed(Input::KEY_A)) {
        // left
        this->position -= this->right * vel;
    }
    if (Input::is_key_pressed(Input::KEY_S)) {
        // backwards
        this->position -= this->front * vel;
    }
    if (Input::is_key_pressed(Input::KEY_D)) {
        // right
        this->position += this->right * vel;
    }

    // Yaw and pitch
    glm::vec2 mouse_delta = Input::get_mouse_position_delta();
    float mouse_x = mouse_delta.x * 0.1f; // mouse sensitivity
    float mouse_y = mouse_delta.y * 0.1f; // mouse sensitivity
    this->yaw += mouse_x;
    this->pitch -= mouse_y;
    if (this->pitch > 89.0f) {
        this->pitch = 89.0f;
    }
    if (this->pitch < -89.0f) {
        this->pitch = - 89.0f;
    }
}

void FlyCamera::notify(const std::string &event_name) {
    float scroll = Input::get_mouse_scroll() * 0.3;
    this->movement_speed += scroll;
    if (this->movement_speed > 20.0f) {
        this->movement_speed = 20.f;
    }
    if (this->movement_speed < 1.0f) {
        this->movement_speed = 1.0f;
    }
}

glm::mat4 FlyCamera::get_view_matrix() const {
    return glm::lookAt(this->position, this->position + this->front, this->up);
}

glm::vec3 FlyCamera::get_position() const {
    return this->position;
}

glm::vec3 FlyCamera::get_view_direction() const {
    return this->front;
}

float FlyCamera::get_fov() const {
    return this->fov;   
}

void FlyCamera::set_fov(float p_fov) {
    this->fov = p_fov;
}
