#pragma once

#include "glm/glm.hpp"

class Light {
    protected:
        glm::vec3 color;
    public:
        virtual glm::vec3 get_color() const = 0;
};