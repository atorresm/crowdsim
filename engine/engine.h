#pragma once

#include "window.h"
#include "environment.h"

class Engine {
    friend class Environment;
    private:
        Engine();
        static Window *window;
    public:
        static void init(int p_window_width, int p_window_height);
        static float delta();
        static void update();
        static bool should_close();
        static void set_clear_color(glm::vec3 p_col);
        static int WINDOW_HEIGHT;
        static int WINDOW_WIDTH;
        ~Engine();
};
