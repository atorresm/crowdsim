#pragma once

#include <vector>
#include <string>
#include <chrono>

using time_point = std::chrono::high_resolution_clock::time_point;

class Benchmark {
public:
	enum TIME_UNIT {
		MICROSECONDS = 0,
		MILLISECONDS = 1,
	};
	Benchmark();
	uint64_t get_elapsed() const;
	inline void start() {
		start_time = std::chrono::high_resolution_clock::now();
	}
	void print_elapsed(const std::string &bench_name, const TIME_UNIT unit = MILLISECONDS);
	void print_elapsed_mean(const std::string &bench_name, const TIME_UNIT unit = MILLISECONDS);

private:
	time_point start_time;
	size_t metrics_index;
	std::vector<uint64_t> metrics;
};
