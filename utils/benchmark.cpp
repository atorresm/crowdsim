#include "benchmark.h"
#include <iostream>

#define MEAN_CACHE_SIZE 20

double unit_conversion_factor[] = {
    1,
    1000,
};

std::string unit_name[] = {
    "us",
    "ms",
};

Benchmark::Benchmark() : start_time(std::chrono::high_resolution_clock::now()), metrics_index(0), metrics(MEAN_CACHE_SIZE) {
}

uint64_t Benchmark::get_elapsed() const {
	time_point end  = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::microseconds>(end - start_time).count();
}

void Benchmark::print_elapsed(const std::string &bench_name, const TIME_UNIT unit) {
    std::cout << bench_name + " time: " << get_elapsed() / unit_conversion_factor[unit] << unit_name[unit] << std::endl;
}

void Benchmark::print_elapsed_mean(const std::string &bench_name, const TIME_UNIT unit) {
	metrics[metrics_index++] = get_elapsed();
	if (metrics_index >= metrics.size()) {
		metrics_index = 0;
	}
	uint64_t mean = 0;
	for (size_t i = 0; i < metrics.size(); i++) {
		mean += metrics[i];
	}
	mean /= metrics.size();
    std::cout << bench_name + " mean time: " << mean / unit_conversion_factor[unit] << unit_name[unit] << std::endl;
}
