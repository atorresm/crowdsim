#version 330 core

layout (location = 0) in vec3 pos;
// ignoring location=1 (vertex color)
layout (location = 2) in vec2 in_uv;

out vec2 uv;

uniform mat4 transform;
uniform mat4 view;
uniform mat4 projection;

void main() {
    gl_Position = projection * view * transform * vec4(pos, 1.0);
    uv = in_uv;
}