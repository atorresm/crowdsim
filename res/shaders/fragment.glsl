#version 330 core

in vec3 normal;
in vec2 uv;
in vec3 frag_pos;
out vec4 color;

struct DirectionalLight {
    vec3 direction;
    vec3 color;
};

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float metallic;
};

uniform DirectionalLight directional_light;
uniform Material material;
uniform vec3 camera_position;
uniform vec3 ambient_color;

vec3 calculate_dir_light() {
    vec3 dir = normalize(-directional_light.direction);
    vec3 norm = normalize(normal);
    // diffuse
    float diff = max(dot(norm, dir), 0.0);
    // specular
    vec3 reflect_dir = reflect(-dir, norm);
    vec3 view_direction = normalize(camera_position - frag_pos);
    float spec = pow(max(dot(view_direction, reflect_dir), 0.0), material.metallic);
    // final combine
    vec3 ambient = ambient_color * vec3(0.3) * texture(material.diffuse, uv).rgb;
    vec3 diffuse = directional_light.color * vec3(0.4) * diff * texture(material.diffuse, uv).rgb;
    vec3 specular = directional_light.color * vec3(0.5) * spec * texture(material.specular, uv).rgb;
    return ambient + diffuse + specular;
}

void main() {
    color = vec4(calculate_dir_light(), 1.0);
}