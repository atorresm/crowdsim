#version 450 core

layout(local_size_x_id = 0) in;
layout(constant_id = 1) const int N = -1; // number of agents
layout(constant_id = 2) const float view_dist = 50.0;
layout(constant_id = 3) const float max_speed = 3.5;
layout(constant_id = 4) const float max_force = 1.0;

// resources
layout(set = 0, binding = 0) readonly buffer in_vx { float vel_x_in[]; };
layout(set = 0, binding = 1) readonly buffer in_vy { float vel_y_in[]; };
layout(set = 0, binding = 2) readonly buffer in_px { float pos_x_in[]; };
layout(set = 0, binding = 3) readonly buffer in_py { float pos_y_in[]; };

layout(set = 0, binding = 4) writeonly buffer out_vx { float vel_x_out[]; };
layout(set = 0, binding = 5) writeonly buffer out_vy { float vel_y_out[]; };
layout(set = 0, binding = 6) writeonly buffer out_px { float pos_x_out[]; };
layout(set = 0, binding = 7) writeonly buffer out_py { float pos_y_out[]; };

// Limit vector length
vec2 limit(float x, float y, float lim) {
    vec2 ret = vec2(x, y);
    float sqlimit = lim*lim;
    float sqmag = x * x + y * y;
    ret = mix(ret, normalize(ret) * lim, step(sqlimit, sqmag)); // ret if sqlimit>sqmag else normalize(ret)*lim
    return ret;
}

vec2 clamp_position(float x, float y) {
    // TODO: parametrize window size
    float w = 800.0;
    float h = 600.0;
    vec2 ret = vec2(x, y);
    float sprite_x = 5.0;
    float sprite_y = 5.0;
    
    if (x > (w + sprite_x)) {
        ret.x = -sprite_x;
    } else if (x < -sprite_x) {
        ret.x = w + sprite_x;
    }
    
    if (y > (h + sprite_y)) {
        ret.y = -sprite_y;
    } else if (y < -sprite_y) {
        ret.y = h + sprite_y;
    }

    return ret;
}

float calc_distance(float ax, float ay, float bx, float by) {
    vec2 a = vec2(ax, ay);
    vec2 b = vec2(bx, by);
    return distance(a, b);
}

void main(void) {
    uint id = gl_GlobalInvocationID.x;

    // Calc forces
    vec2 alignment = vec2(0.0);
    vec2 separation = vec2(0.0);
    vec2 cohesion = vec2(0.0);
    int neighbours_number = 0;
    for (int i = 0; i < N; i++) {
        if (id == i) {
            continue;
        }
        float d = calc_distance(pos_x_in[id], pos_y_in[id], pos_x_in[i], pos_y_in[i]);
        if (d < view_dist) {
            neighbours_number += 1;
            alignment += vec2(vel_x_in[i], vel_y_in[i]);
            cohesion += vec2(pos_x_in[i], pos_y_in[i]);
            vec2 sep_diff = vec2(pos_x_in[id], pos_y_in[id]) - vec2(pos_x_in[i], pos_y_in[i]);
            sep_diff /= d * d;
            separation += sep_diff;
        }
    }
    if (neighbours_number > 0) {
        // Alignment
        alignment /= neighbours_number;
        alignment = normalize(alignment);
        alignment *= max_speed;
        alignment -= vec2(vel_x_in[id], vel_y_in[id]);
        alignment = limit(cohesion.x, cohesion.y, max_force);

        // Cohesion
        cohesion /= neighbours_number;
        cohesion -= vec2(pos_x_in[id], pos_y_in[id]);
        cohesion = normalize(cohesion);
        cohesion *= max_speed;
        cohesion -= vec2(vel_x_in[id], vel_y_in[id]);
        cohesion = limit(cohesion.x, cohesion.y, max_force);

        // Separation
        separation /= neighbours_number;
        separation = normalize(separation);
        separation *= max_speed;
        separation -= vec2(vel_x_in[id], vel_y_in[id]);
        separation = limit(separation.x, separation.y, max_force);
    }

    // Sum forces
    vec2 accel = alignment + cohesion + separation;

    // Update position
    vec2 pos_clamped = vec2(0.0f);
    pos_clamped.x = pos_x_in[id] + vel_x_in[id];
    pos_clamped.y = pos_y_in[id] + vel_y_in[id];
    pos_clamped = clamp_position(pos_clamped.x, pos_clamped.y);
    pos_x_out[id] = pos_clamped.x;
    pos_y_out[id] = pos_clamped.y;
    
    // Update velocity
    vec2 vel_limited = limit(vel_x_in[id] + accel.x, vel_y_in[id] + accel.y, max_speed);
    vel_x_out[id] = vel_limited.x;
    vel_y_out[id] = vel_limited.y;
}