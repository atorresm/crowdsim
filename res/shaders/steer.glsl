#version 450 core

layout(local_size_x_id = 0) in;
layout(constant_id = 1) const int n_agents= -1;
layout(constant_id = 2) const int n_obstacles = -1;
layout(constant_id = 3) const float view_dist = 50.0;
layout(constant_id = 4) const float max_speed = 3.5;
layout(constant_id = 5) const float max_force = 1.0;

// resources
layout(set = 0, binding = 0) readonly buffer in_vx { float vel_x_in[]; };
layout(set = 0, binding = 1) readonly buffer in_vy { float vel_y_in[]; };
layout(set = 0, binding = 2) readonly buffer in_px { float pos_x_in[]; };
layout(set = 0, binding = 3) readonly buffer in_py { float pos_y_in[]; };
layout(set = 0, binding = 4) readonly buffer in_tx { float target_x_in[]; };
layout(set = 0, binding = 5) readonly buffer in_ty { float target_y_in[]; };
layout(set = 0, binding = 6) readonly buffer in_rad { float radius[]; };
layout(set = 0, binding = 7) readonly buffer in_opx { float obst_pos_x_in[]; };
layout(set = 0, binding = 8) readonly buffer in_opy { float obst_pos_y_in[]; };
layout(set = 0, binding = 9) readonly buffer in_orad { float obst_radius[]; };

layout(set = 0, binding = 10) writeonly buffer out_vx { float vel_x_out[]; };
layout(set = 0, binding = 11) writeonly buffer out_vy { float vel_y_out[]; };
layout(set = 0, binding = 12) writeonly buffer out_px { float pos_x_out[]; };
layout(set = 0, binding = 13) writeonly buffer out_py { float pos_y_out[]; };

// Limit vector length
vec2 limit(float x, float y, float lim) {
    vec2 ret = vec2(x, y);
    if (length(ret) > lim) {
        return normalize(ret) * lim;
    } else {
        return ret;
    }
}

float calc_distance(float ax, float ay, float bx, float by) {
    vec2 a = vec2(ax, ay);
    vec2 b = vec2(bx, by);
    return distance(a, b);
}

void main(void) {
    uint id = gl_GlobalInvocationID.x;

    // Calc forces
    vec2 separation = vec2(0.0);
    int neighbours_number = 0;
    // Compute agents
    for (int i = 0; i < n_agents; i++) {
        if (id == i) {
            continue;
        }
        float d = calc_distance(pos_x_in[id], pos_y_in[id], pos_x_in[i], pos_y_in[i]) - radius[id] - radius[i];
        if (d < view_dist) {
            neighbours_number += 1;
            vec2 sep_diff = vec2(pos_x_in[id], pos_y_in[id]) - vec2(pos_x_in[i], pos_y_in[i]);
            separation += sep_diff;
        }
    }
    // Compute obstacles
    for (int i = 0; i < n_obstacles; i++) {
        float d = calc_distance(pos_x_in[id], pos_y_in[id], obst_pos_x_in[i], obst_pos_y_in[i]) - radius[i] - obst_radius[i];
        if (d < view_dist) {
            neighbours_number += 1;
            vec2 sep_diff = vec2(pos_x_in[id], pos_y_in[id]) - vec2(obst_pos_x_in[i], obst_pos_y_in[i]);
            //sep_diff /= d * d;
            separation += sep_diff;
        }
    }
    if (neighbours_number > 0) {
        // Separation
        separation /= neighbours_number;
        separation *= max_speed;
        separation -= vec2(vel_x_in[id], vel_y_in[id]);
        separation = limit(separation.x, separation.y, max_force);
    }
    // Seek force
    vec2 seek = vec2(0.0);
    vec2 target = vec2(target_x_in[id], target_y_in[id]);
    vec2 pos = vec2(pos_x_in[id], pos_y_in[id]);
    seek = normalize(target - pos) * max_speed;
    seek -= vec2(vel_x_in[id], vel_y_in[id]);
    seek = limit(seek.x, seek.y, max_force);

    // Sum forces
    vec2 accel = separation * 0.5f + 0.15f * seek;

    // Update position
    pos_x_out[id] = pos_x_in[id] + vel_x_in[id];
    pos_y_out[id] = pos_y_in[id] + vel_y_in[id];
    
    // Update velocity
    vec2 vel_limited = limit(vel_x_in[id] + accel.x, vel_y_in[id] + accel.y, max_speed);
    vel_x_out[id] = vel_limited.x;
    vel_y_out[id] = vel_limited.y;
}