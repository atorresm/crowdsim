#include "gpuflock.h"
#include "vuda/vuda.hpp"
#include "../../engine/shader.h"
#include <random>
#include <ctime>
#include <iostream>

GPUFlock::GPUFlock(int p_n_agents, float p_neighbour_radius) {
    this->neighbour_radius = p_neighbour_radius;
    this->n_agents = p_n_agents;

    // Setup Vulkan device
    /// TODO: check for devices
    vuda::setDevice(0);

    // Initialize arrays
    this->vel_x = new float[this->n_agents];
    this->vel_y = new float[this->n_agents];
    this->pos_x = new float[this->n_agents];
    this->pos_y = new float[this->n_agents];

    vuda::malloc((void**) &this->dev_vel_x, this->n_agents * sizeof(float));
    vuda::malloc((void**) &this->dev_vel_y, this->n_agents * sizeof(float));
    vuda::malloc((void**) &this->dev_pos_x, this->n_agents * sizeof(float));
    vuda::malloc((void**) &this->dev_pos_y, this->n_agents * sizeof(float));

    vuda::malloc((void**) &this->dev_out_vel_x, this->n_agents * sizeof(float));
    vuda::malloc((void**) &this->dev_out_vel_y, this->n_agents * sizeof(float));
    vuda::malloc((void**) &this->dev_out_pos_x, this->n_agents * sizeof(float));
    vuda::malloc((void**) &this->dev_out_pos_y, this->n_agents * sizeof(float));
    
    // Create random starting velocities and positions
    std::default_random_engine gen(std::time(nullptr));
    /// TODO: Use parametrized screen size (singleton?)
    std::uniform_real_distribution<float> dist_w(0, 800);
    std::uniform_real_distribution<float> dist_h(0, 600);
    std::uniform_real_distribution<float> dist_len(2, 5); // random magnitude for velocity
    std::uniform_real_distribution<float> dist_vel(-1.0, 1.0);
    
    this->sprites.reserve(this->n_agents);    

    for (size_t i = 0; i < this->n_agents; i++) {
        this->vel_x[i] = dist_vel(gen) * dist_len(gen);
        this->vel_y[i] = dist_vel(gen) * dist_len(gen);
        this->pos_x[i] = dist_w(gen);
        this->pos_y[i] = dist_h(gen);
        this->sprites[i] = new Sprite("res/textures/flockagent.jpg", glm::vec2(5, 5));
    }
    
    Shader::compile_glsl_file_to_spv_file(Shader::ShaderType::COMPUTE, "res/shaders/flocking.glsl", "res/shaders/flocking.spv", true);
}

GPUFlock::~GPUFlock() {
    delete [] this->vel_x;
    delete [] this->vel_y;
    delete [] this->pos_x;
    delete [] this->pos_y;

    vuda::free(this->dev_vel_x);
    vuda::free(this->dev_vel_y);
    vuda::free(this->dev_pos_x);
    vuda::free(this->dev_pos_y);

    vuda::free(this->dev_out_vel_x);
    vuda::free(this->dev_out_vel_y);
    vuda::free(this->dev_out_pos_x);
    vuda::free(this->dev_out_pos_y);

    for (size_t i = 0; i < this->sprites.size(); i++) {
        if (this->sprites[i] != nullptr) {
            delete this->sprites[i];
        }
    }
    
}

void GPUFlock::draw(float p_delta) {
    const int N = this->n_agents;
    const int stream_id = 0;
    const int blocks = std::ceil(N / 256.f);
    const int threads = 256;

    vuda::memcpy(this->dev_vel_x, this->vel_x, N * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_vel_y, this->vel_y, N * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_pos_x, this->pos_x, N * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_pos_y, this->pos_y, N * sizeof(float), vuda::memcpyHostToDevice);

    // Launch kernel
    vuda::launchKernel("res/shaders/flocking.spv", "main", stream_id, blocks, threads,
        this->dev_vel_x, this->dev_vel_y, 
        this->dev_pos_x, this->dev_pos_y,
        this->dev_out_vel_x, this->dev_out_vel_y,
        this->dev_out_pos_x, this->dev_out_pos_y, N,
        this->neighbour_radius, this->max_speed, this->max_force
    );

    // Get results back
    vuda::memcpy(this->vel_x, this->dev_out_vel_x, N * sizeof(float), vuda::memcpyDeviceToHost);
    vuda::memcpy(this->vel_y, this->dev_out_vel_y, N * sizeof(float), vuda::memcpyDeviceToHost);
    vuda::memcpy(this->pos_x, this->dev_out_pos_x, N * sizeof(float), vuda::memcpyDeviceToHost);
    vuda::memcpy(this->pos_y, this->dev_out_pos_y, N * sizeof(float), vuda::memcpyDeviceToHost);

    // Draw actual sprites
    for (size_t i = 0; i < this->n_agents; i++) {
        this->sprites[i]->set_position(glm::vec2(this->pos_x[i], this->pos_y[i]));
        this->sprites[i]->draw(p_delta);
    }
}
