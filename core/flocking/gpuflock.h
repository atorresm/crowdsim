#pragma once

#include <vector>
#include "../../engine/sprite.h"

class GPUFlock {
    private:
        float neighbour_radius;
        int n_agents;
        const float max_speed = 3.5f;
        const float max_force = 1.0f;
        
        // Arrays for processing
        float *vel_x;
        float *vel_y;
        float *pos_x;
        float *pos_y;

        float *dev_vel_x;
        float *dev_vel_y;
        float *dev_pos_x;
        float *dev_pos_y;

        float *dev_out_vel_x;
        float *dev_out_vel_y;
        float *dev_out_pos_x;
        float *dev_out_pos_y;

        std::vector<Sprite*> sprites;

    public:
        GPUFlock(int p_n_agents = 256, float p_neighbour_radius = 50.0);
        ~GPUFlock();
        void draw(float p_delta);
};
