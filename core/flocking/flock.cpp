#include "flock.h"
#include <random>
#include <iostream>
#include <ctime>

Flock::Flock(int p_n_agents, int p_neighbor_radius) {
    // Create agents with random starting positions
    std::default_random_engine gen(std::time(nullptr));
    // TODO: Use parametrized screen size (singleton?)
    std::uniform_int_distribution<int> dist_w(0, 800);
    std::uniform_int_distribution<int> dist_h(0, 600);
    std::uniform_real_distribution<float> dist_len(2, 5); // random magnitude for velocity
    std::uniform_real_distribution<float> dist_vel(-1.0, 1.0);
    for (size_t i = 0; i < p_n_agents; i++) {
        float x = (float) dist_w(gen);
        float y = (float) dist_h(gen);
        float xvel = dist_vel(gen) * dist_len(gen);
        float yvel = dist_vel(gen) * dist_len(gen);
        FlockAgent *agent = new FlockAgent(i, glm::vec2(x, y), glm::vec2(xvel, yvel));
        this->agents.push_back(agent);
    }
    this->neighbour_radius = p_neighbor_radius;
}

Flock::~Flock() {
    for (size_t i = 0; i < this->agents.size(); i++) {
        if (this->agents[i] != nullptr) {
            delete this->agents[i];
        }
    }
}

std::vector<FlockAgent*> Flock::get_neighbours(const FlockAgent *p_agent) {
    std::vector<FlockAgent*> ret;
    for (size_t i = 0; i < this->agents.size(); i++) {
        FlockAgent *current_agent = this->agents[i];
        if (p_agent->get_id() != current_agent->get_id() && glm::distance(current_agent->get_position(), p_agent->get_position()) <= this->neighbour_radius) {
            ret.push_back(current_agent);
        }
    }
    return ret;
}

void Flock::draw(float p_delta) {
    //#pragma omp parallel for
    for (size_t i = 0; i < this->agents.size(); i++) {
        agents[i]->update(this->get_neighbours(agents[i]));
        agents[i]->draw(p_delta);
    }
}