#include "flockagent.h"


FlockAgent::FlockAgent(int p_id, glm::vec2 p_pos, glm::vec2 p_vel) {
    // TODO: arrow texture
    this->sprite = new Sprite("res/textures/flockagent.jpg", glm::vec2(5, 5));
    this->id = p_id;
    this->position = p_pos;
    this->velocity = p_vel;
    this->acceleration = glm::vec2(0.0);
}

FlockAgent::~FlockAgent() {
    if (this->sprite != nullptr) {
        delete this->sprite;
    }
}

void FlockAgent::clamp_position() {
    // TODO: parametrize window size
    int w = 800;
    int h = 600;
    int sprite_x = this->sprite->get_size().x;
    int sprite_y = this->sprite->get_size().y;
    if (this->position.x > (w + sprite_x)) {
        this->position.x = -sprite_x; 
    } else if (this->position.x < -sprite_x) {
        this->position.x = w + sprite_x;
    }
    if (this->position.y > (h + sprite_y)) {
        this->position.y = -sprite_y;
    } else if (this->position.y < -sprite_y) {
        this->position.y = h + sprite_y;
    }
}

glm::vec2 FlockAgent::limit_vector(const glm::vec2 &p_vec, float limit) {
    glm::vec2 ret = glm::vec2(p_vec);
    float square_limit = limit * limit;
    float squared_magnitude = ret.x * ret.x + ret.y * ret.y;
    if (squared_magnitude > square_limit) {
        ret = glm::normalize(ret) * limit;
    }
    return ret;
}

glm::vec2 FlockAgent::calc_alignment(const std::vector<FlockAgent*> &p_neighbours) {
    if (p_neighbours.size() == 0) {
        return glm::vec2(0.0);
    }

    // Calc mean velocity
    glm::vec2 steering_force = glm::vec2(0.0);
    for (size_t i = 0; i < p_neighbours.size(); i++) {
        steering_force += p_neighbours[i]->get_velocity();
    }
    steering_force /= p_neighbours.size();
    steering_force = glm::normalize(steering_force);
    steering_force *= this->MAX_SPEED; // Maintain full speed
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    
    return steering_force;
}

glm::vec2 FlockAgent::calc_separation(const std::vector<FlockAgent*> &p_neighbours) {
    if (p_neighbours.size() == 0) {
        return glm::vec2(0.0);
    }

    glm::vec2 steering_force = glm::vec2(0.0);
    for (size_t i = 0; i < p_neighbours.size(); i++) {
        float d = glm::distance(this->position, p_neighbours[i]->get_position());
        glm::vec2 difference = this->position - p_neighbours[i]->get_position();
        difference /= d * d;
        steering_force += difference;
    }
    steering_force /= p_neighbours.size();
    steering_force = glm::normalize(steering_force);
    steering_force *= this->MAX_SPEED; // Maintain full speed
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    
    return steering_force;
}

glm::vec2 FlockAgent::calc_cohesion(const std::vector<FlockAgent*> &p_neighbours) {
    if (p_neighbours.size() == 0) {
        return glm::vec2(0.0);
    }

    // Calc mean position
    glm::vec2 steering_force = glm::vec2(0.0);
    for (size_t i = 0; i < p_neighbours.size(); i++) {
        steering_force += p_neighbours[i]->get_position();
    }
    steering_force /= p_neighbours.size();
    steering_force -= this->position;
    steering_force = glm::normalize(steering_force);
    steering_force *= this->MAX_SPEED; // Maintain full speed
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    
    return steering_force;
}

glm::vec2 FlockAgent::get_position() const {
    return this->position;
}

void FlockAgent::set_position(const glm::vec2 &p_pos) {
    this->position = p_pos;
}

glm::vec2 FlockAgent::get_velocity() const {
    return this->velocity;
}

void FlockAgent::set_velocity(const glm::vec2 &p_vel) {
    this->velocity = p_vel;
}

glm::vec2 FlockAgent::get_acceleration() const {
    return this->acceleration;
}

void FlockAgent::set_acceleration(const glm::vec2 &p_acc) {
    this->acceleration = p_acc;
}

int FlockAgent::get_id() const {
    return this->id;
}

void FlockAgent::update(const std::vector<FlockAgent*> &p_neighbours) {
    this->clamp_position();

    glm::vec2 alignment = this->calc_alignment(p_neighbours);
    glm::vec2 separation = this->calc_separation(p_neighbours);
    glm::vec2 cohesion = this->calc_cohesion(p_neighbours);
    this->acceleration += 0.15f * alignment + 0.24f * separation + 0.2f * cohesion;

    this->position += this->velocity;
    this->velocity += this->acceleration;
    this->velocity = this->limit_vector(this->velocity, this->MAX_SPEED);
    this->acceleration = glm::vec2(0.0); // reset acceleration
}

void FlockAgent::draw(float p_delta) {
    // TODO: rotation
    this->sprite->set_position(this->position);
    this->sprite->draw(p_delta);
}