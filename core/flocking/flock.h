#pragma once

#include <vector>
#include "flockagent.h"

class Flock {
    private:
        int neighbour_radius;
        std::vector<FlockAgent*> agents;
        std::vector<FlockAgent*> get_neighbours(const FlockAgent *p_agent);
    public:
        Flock(int p_n_agents = 200, int p_neighbour_radius = 50);
        ~Flock();
        void draw(float p_delta);
};