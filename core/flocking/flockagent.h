#pragma once

#include <vector>
#include <memory>
#include "glm/glm.hpp"
#include "../../engine/sprite.h"

class FlockAgent {
    private:
        const float MAX_SPEED = 3.5;
        const float MAX_FORCE = 1.0;
        int id;
        glm::vec2 position;
        glm::vec2 velocity;
        glm::vec2 acceleration;
        Sprite *sprite;
        void clamp_position();
        glm::vec2 calc_alignment(const std::vector<FlockAgent*> &p_neighbours);
        glm::vec2 calc_separation(const std::vector<FlockAgent*> &p_neighbours);
        glm::vec2 calc_cohesion(const std::vector<FlockAgent*> &p_neighbours);
        glm::vec2 limit_vector(const glm::vec2 &p_vec, float limit);
    public:
        FlockAgent(int p_id, glm::vec2 p_pos, glm::vec2 p_vel);
        ~FlockAgent();
        glm::vec2 get_position() const;
        void set_position(const glm::vec2 &p_pos);
        glm::vec2 get_velocity() const;
        void set_velocity(const glm::vec2 &p_vel);
        glm::vec2 get_acceleration() const;
        void set_acceleration(const glm::vec2 &p_acc);
        int get_id() const;
        void update(const std::vector<FlockAgent*> &p_neighbours);
        void draw(float p_delta);
        inline bool operator==(const FlockAgent &b) { return this->id == b.get_id(); };
        inline bool operator!=(const FlockAgent &b) { return this->id != b.get_id(); };
};
