#include "../engine/engine.h"
#include "flocking/gpuflock.h"
#include "orca/orcasimulation.h"
#include "steer/steersimulation.h"
#include "steer/gpusteersimulation.h"
#include "flocking/flock.h"
#include "../utils/benchmark.h"


#include <chrono>
#include <thread>

int main(int argc, char const *argv[]) {
    Engine::init(800, 600);
    Engine::set_clear_color(glm::vec3(0.1));
    // ORCA Simulation
    
    //std::vector<glm::vec2> positions = {
    //    glm::vec2(10, 10),
    //    glm::vec2(-10, 10),
    //    glm::vec2(10, -10),
    //    glm::vec2(-10, -10),
    //};
    //std::vector<glm::vec2> targets = {
    //    glm::vec2(-10, -10),
    //    glm::vec2(10, -10),
    //    glm::vec2(-10, 10),
    //    glm::vec2(10, 10),
    //};
    //ORCASimulation sim(positions, targets);

    // Steer simulation

    std::vector<glm::vec2> positions = {};
    std::vector<glm::vec2> targets = {};
    for (size_t i = 0; i < 300; i++) {
        if (i < 100) {
            positions.push_back(glm::vec2(10, 5 * i));
            targets.push_back(glm::vec2(700, 5 * i));
        } else if (i < 200) {
            positions.push_back(glm::vec2(20, 5 * i));
            targets.push_back(glm::vec2(710, 5 * i));
        } else if (i <= 300) {
            positions.push_back(glm::vec2(30, 5 * i));
            targets.push_back(glm::vec2(720, 5 * i));
        }
    }
    
    GPUSteerSimulation sim(positions, targets, 30, true);
    for (size_t i = 0; i < 70; i++) {
        if (i >= 25 && i <= 35) {
            continue;
        }
        if (i < 25) {
            sim.add_circular_obstacle(glm::vec2(300 + i * 4.5, i * 10), 5.0);
        }
        if (i > 35) {
            sim.add_circular_obstacle(glm::vec2(550 - i * 4.5, i * 10), 5.0);
        }
    }

    for (size_t i = 0; i < 10; i++) {
        sim.add_circular_obstacle(glm::vec2(500, 250 + 10*i), 5.0);
    }
//
    ////GPUFlock flock;
    //Flock flock(64*2*2*2*2*2*2*2*2, 50);
    while (!Engine::should_close()) {
        float delta = Engine::delta();
        
        // Main loop here
        sim.update(delta);
        sim.draw(delta);
        //flock.draw(delta);
        //std::cout << delta << "\n";
        Engine::update();
    }

    return 0;
}
