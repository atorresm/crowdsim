#pragma once

#include <vector>
#include "glm/glm.hpp"
#include "orcaagent.h"
#include "orcapointobstacle.h"
#include "../engine/scene.h"

class ORCASimulation {
    private:
        std::vector<ORCAAgent*> agents;
        std::vector<ORCAPointObstacle*> obstacles;
        Scene *scn;
        std::vector<Model*> agent_models;
        std::vector<Model*> obstacle_models;
    public:
        ORCASimulation() {};
        ORCASimulation(std::vector<glm::vec2> agent_positions, std::vector<glm::vec2> agent_targets);
        ~ORCASimulation();
        void update(float p_delta);
        void draw(float p_delta);
        void add_obstacle(std::vector<glm::vec2> p_vertices);
        std::vector<ORCAAgent*> get_agent_neigbors(ORCAAgent *p_agent);
        std::vector<ORCAPointObstacle*> get_agent_nearby_obstacles(ORCAAgent *p_agent);
        bool is_finished();
};
