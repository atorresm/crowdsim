#pragma once

#include "glm/glm.hpp"

struct DirectedLine {
    glm::vec2 origin;
    glm::vec2 direction;
};