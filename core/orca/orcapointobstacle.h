#pragma once

#include "glm/glm.hpp"

// Struct holding Obstacle info for a single obstacle-point

struct ORCAPointObstacle {
    glm::vec2 position;
    ORCAPointObstacle *next;
    ORCAPointObstacle *previous;
    bool is_convex;
    glm::vec2 direction;
    int index;
};