#include "orcaagent.h"

ORCAAgent::ORCAAgent(glm::vec2 p_pos, glm::vec2 p_target) {
    this->position = p_pos;
    this->target = p_target;
    this->velocity = glm::vec2(0);
    this->preferred_velocity = glm::normalize(this->target - this->position);
    // TODO parametrize
    this->radius = 2.0f;
    this->time_horizon = 6.0f;
    this->neighbor_view_distance = 15.0f;
    this->max_speed = 5.0f;
}

ORCAAgent::~ORCAAgent() {
   
}

void ORCAAgent::update(std::vector<ORCAAgent*> p_neighbors, std::vector<ORCAPointObstacle*> p_nearby_obstacles, float p_delta) {
    // Step 1: set preferred velocity
    this->preferred_velocity = glm::normalize(this->target - this->position);

    // Step 2: compute ORCA lines for avoiding obstacles and neighbors
    std::vector<DirectedLine> lines;
    
    // Step 2.1: compute ORCA lines for obstacle avoidance
    for (size_t i = 0; i < p_nearby_obstacles.size(); i++) {
        ORCAPointObstacle *curr = p_nearby_obstacles[i];
        ORCAPointObstacle *next = curr->next;
        glm::vec2 pos_diff_curr = curr->position - this->position;
        glm::vec2 pos_diff_next = next->position - this->position;

        // Check if obstacle has been covered by previous ORCA line
        bool covered = false;
        for (size_t j = 0; j < lines.size(); j++) {
            float det_curr = this->calculate_2d_determinant((1.f / this->time_horizon) * pos_diff_curr - lines[j].origin, lines[j].direction);
            float det_next = this->calculate_2d_determinant((1.f / this->time_horizon) * pos_diff_next - lines[j].origin, lines[j].direction);
            if (((det_curr - (1.f / this->time_horizon) * this->radius) >= -0.00001f) && ((det_next - (1.f / this->time_horizon) * this->radius) >= -0.00001f)) {
                covered = true;
                break;
            }
        }
        if (covered) {
            continue;
        }

        float dist_curr = glm::dot(pos_diff_curr, pos_diff_curr);
        float dist_next = glm::dot(pos_diff_next, pos_diff_next);
        float radius_sq = this->radius * this->radius;
        glm::vec2 obst_vec = next->position - curr->position;
        float s = glm::dot(-pos_diff_curr, obst_vec) / glm::dot(obst_vec, obst_vec);
        float dist_line_sq = glm::dot(-pos_diff_curr - s * obst_vec, -pos_diff_curr - s * obst_vec);
        DirectedLine line;
        
        // Handle collision in different sides (curr obst, next obst, segment obst)
        if (s < 0.f && dist_curr <= radius_sq) {
            if (curr->is_convex) {
                line.origin = glm::vec2(0);
                line.direction = glm::normalize(glm::vec2(-pos_diff_curr.y, pos_diff_curr.x));
                lines.push_back(line);
            }
            continue;
        } else if (s > 1.f && dist_next <= radius_sq) {
            if (next->is_convex && (this->calculate_2d_determinant(pos_diff_next, next->direction) >= 0.0f)) {
                line.origin = glm::vec2(0);
                line.direction = glm::normalize(glm::vec2(-pos_diff_next.y, pos_diff_next.x));
                lines.push_back(line);
            }
            continue;
        } else if (s >= 0.f && s <= 1.f && dist_line_sq <= radius_sq) {
            line.origin = glm::vec2(0);
            line.direction = -curr->direction;
            lines.push_back(line);
            continue;
        }

        // No collision, different cases
        glm::vec2 left = glm::vec2(0);
        glm::vec2 right = glm::vec2(0);
        if (s < 0.0f && dist_line_sq <= radius_sq) {
            if (!curr->is_convex) {
                continue;
            }
            next = curr;
            float side = std::sqrt(dist_curr - radius_sq);
            left = glm::vec2(pos_diff_curr.x * side - pos_diff_curr.y * this->radius, pos_diff_curr.x * this->radius + pos_diff_curr.y * side) / dist_curr;
            right = glm::vec2(pos_diff_curr.x * side + pos_diff_curr.y * this->radius, -pos_diff_curr.x * this->radius + pos_diff_curr.y * side) / dist_curr;
        } else if (s > 1.f && dist_line_sq <= radius_sq) {
            if (!next->is_convex) {
                continue;
            }
            curr = next;
            float side = std::sqrt(dist_next - radius_sq);
            left = glm::vec2(pos_diff_next.x * side - pos_diff_next.y * this->radius, pos_diff_next.x * this->radius + pos_diff_next.y * side) / dist_next;
            right = glm::vec2(pos_diff_next.x * side + pos_diff_next.y * this->radius, -pos_diff_next.x * this->radius + pos_diff_next.y * side) / dist_next;
        } else {
            if (curr->is_convex) {
                float side = std::sqrt(dist_curr - radius_sq);
                left = glm::vec2(pos_diff_curr.x * side - pos_diff_curr.y * this->radius, pos_diff_curr.x * this->radius + pos_diff_curr.y * side) / dist_curr;
            } else {
                left = -curr->direction;
            }

            if (next->is_convex) {
                float side = std::sqrt(dist_next - radius_sq);
                right = glm::vec2(pos_diff_next.x * side + pos_diff_next.y * this->radius, -pos_diff_next.x * this->radius + pos_diff_next.y * side) / dist_next;
            } else {
                right = curr->direction;
            }
        }

        ORCAPointObstacle *prev = curr->previous;
        bool no_constraint_left = false;
        bool no_constraint_right = false;

        if (curr->is_convex && this->calculate_2d_determinant(left, -prev->direction) >= 0.f) {
            left = -prev->direction;
            no_constraint_left = true;
        }

        if (next->is_convex && this->calculate_2d_determinant(right, next->direction) <= 0.f) {
            right = next->direction;
            no_constraint_right = true;
        }

        glm::vec2 left_cutoff = (1.f / this->time_horizon) * (curr->position - this->position);
        glm::vec2 right_cutoff = (1.f / this->time_horizon) * (next->position - this->position);
        glm::vec2 cutoff_diff = right_cutoff - left_cutoff;

        float t = curr == next ? 0.5f : glm::dot(this->velocity - left_cutoff, cutoff_diff) / glm::dot(cutoff_diff, cutoff_diff);
        float t_left = glm::dot(this->velocity - left_cutoff, left);
        float t_right = glm::dot(this->velocity - right_cutoff, right);

        if ((t < 0.f && t_left < 0.f) || (curr == next && t_left < 0.f && t_right < 0.f)) {
            glm::vec2 w = glm::normalize(this->velocity - left_cutoff);
            line.direction = glm::vec2(w.y, -w.x);
            line.origin = left_cutoff + this->radius * (1.f / this->time_horizon) * w;
            lines.push_back(line);
            continue;
        } else if (t > 1.f && t_right < 0.f) {
            glm::vec2 w = glm::normalize(this->velocity - right_cutoff);
            line.direction = glm::vec2(w.y, -w.x);
            line.origin = right_cutoff + this->radius * (1.f / this->time_horizon) * w;
            lines.push_back(line);
            continue;
        }

        float dist_cutoff_sq = (t < 0.f || t > 1.f || curr == next) ? 
            std::numeric_limits<float>::infinity() : 
            glm::dot(this->velocity - (left_cutoff + t * cutoff_diff), this->velocity - (left_cutoff + t * cutoff_diff));
        float dist_left_sq = t_left < 0.f ? 
            std::numeric_limits<float>::infinity() : 
            glm::dot(this->velocity - (left_cutoff + t_left * left), this->velocity - (left_cutoff + t_left * left));
        float dist_right_sq = t_right < 0.f ? 
            std::numeric_limits<float>::infinity() : 
            glm::dot(this->velocity - (right_cutoff + t_right * right), this->velocity - (right_cutoff + t_right * right));

        if (dist_cutoff_sq <= dist_left_sq && dist_cutoff_sq <= dist_right_sq) {
            line.direction = -curr->direction;
            line.origin = left_cutoff + this->radius * (1.f / this->time_horizon) * glm::vec2(-line.direction.y, line.direction.x);
            lines.push_back(line);
            continue;
        } else if (dist_left_sq <= dist_right_sq) {
            if (no_constraint_left) {
                continue;
            }
            line.direction = left;
            line.origin = left_cutoff + this->radius * (1.f / this->time_horizon) * glm::vec2(-line.direction.y, line.direction.x);
            lines.push_back(line);
            continue;
        } else {
            if (no_constraint_right) {
                continue;
            }
            line.direction = -right;
            line.origin = right_cutoff + this->radius * (1.f / this->time_horizon) * glm::vec2(-line.direction.y, line.direction.x);
            lines.push_back(line);
            continue;
        }
    }

    // only obstacle lines added up until now
    int num_obst_lines = lines.size();
    
    // Step 2.2: compute ORCA lines for neighbor avoidance
    for (size_t i = 0; i < p_neighbors.size(); i++) {
        ORCAAgent *neighbor = p_neighbors[i];
        glm::vec2 pos_diff = neighbor->get_position() - this->get_position();
        glm::vec2 vel_diff = this->get_velocity() - neighbor->get_velocity();
        float dist_sq = glm::dot(pos_diff, pos_diff);
        float radius_sum = this->get_radius() + neighbor->get_radius();
        float radius_sum_sq = radius_sum * radius_sum;

        DirectedLine line;
        glm::vec2 u; // (v_left, v_right)

        if (dist_sq > radius_sum_sq) {
            // No collision
            glm::vec2 w = vel_diff - (1.0f / this->time_horizon) * pos_diff;
            float w_length_sq = glm::dot(w, w);
            float w_length = std::sqrt(w_length_sq);
            float pos_dot = glm::dot(w, pos_diff);

            // Decide line direction
            if (pos_dot < 0.0f && (pos_dot * pos_dot) > radius_sum_sq + w_length_sq) {
                glm::vec2 unit_w = w / w_length;
                line.direction = glm::vec2(unit_w.y, -unit_w.x);
                u = unit_w * (radius_sum * (1.0f / this->time_horizon) - w_length);
            } else {
                float side = std::sqrt(dist_sq - radius_sum_sq);
                float det = calculate_2d_determinant(pos_diff, w);
                if (det > 0.0f) {
                    line.direction = glm::vec2(pos_diff.x * side - pos_diff.y * radius_sum, pos_diff.x * radius_sum + pos_diff.y * side) / dist_sq;
                } else {
                    line.direction = -glm::vec2(pos_diff.x * side + pos_diff.y * radius_sum, -pos_diff.x * radius_sum + pos_diff.y * side) / dist_sq;
                }
                u = glm::dot(vel_diff, line.direction) * line.direction - vel_diff;
            }
        } else {
            // Collision
            glm::vec2 w = vel_diff - (1.0f / p_delta) * pos_diff;
            float w_length = std::sqrt(glm::dot(w, w));
            glm::vec2 unit_w = w / w_length;
            line.direction = glm::vec2(unit_w.y, -unit_w.x);
            u = (radius_sum * (1.0f / p_delta) - w_length) * unit_w;
        }

        line.origin = this->get_velocity() + u * 0.5f;
        lines.push_back(line);
    }

    // Step 3: get optimal new velocity using linear programming
    glm::vec2 new_vel;
    int line_failed = this->run_linear_program_2(lines, this->preferred_velocity, false, new_vel);
    if (line_failed < lines.size()) {
        this->run_linear_program_3(lines, num_obst_lines, line_failed, new_vel);
    }

    // Step 4: update agent
    this->velocity = new_vel;
    this->position += this->velocity * p_delta;
}

bool ORCAAgent::run_linear_program_1(std::vector<DirectedLine> p_lines, int p_current_line, glm::vec2 opt_velocity, bool optimize_direction, glm::vec2 &result) {
    float dot = glm::dot(p_lines[p_current_line].origin, p_lines[p_current_line].direction);
    float discriminant = (dot * dot) + (radius * radius) - glm::dot(p_lines[p_current_line].origin, p_lines[p_current_line].origin); // dot = sq length

    if (discriminant < 0.0f) {
        return false;
    }
    float sqr_disc = std::sqrt(discriminant);
    float left = -dot - sqr_disc;
    float right = -dot + sqr_disc;

    for (size_t i = 0; i < p_current_line; i++) {
        float den = this->calculate_2d_determinant(p_lines[p_current_line].direction, p_lines[i].direction);
        float num = this->calculate_2d_determinant(p_lines[i].direction, p_lines[p_current_line].origin - p_lines[i].origin);
        if (std::fabs(den) < 0.00001f) {
            // lines are almost parallel
            if (num < 0.0f) {
                return false;
            } else {
                continue;
            }
        }

        float t = num / den;

        if (den >= 0.0f) {
            right = std::min(right, t);
        } else {
            left = std::max(left, t);
        }

        if (left > right) {
            return false;
        }
    }

    if (optimize_direction) {
        if (glm::dot(opt_velocity, p_lines[p_current_line].direction) > 0.0f) {
            result = p_lines[p_current_line].origin + right * p_lines[p_current_line].direction;
        } else {
            result = p_lines[p_current_line].origin + left * p_lines[p_current_line].direction;
        }
    } else {
        float t = glm::dot(p_lines[p_current_line].direction, opt_velocity - p_lines[p_current_line].origin);
        if (t < left) {
            result = p_lines[p_current_line].origin + left * p_lines[p_current_line].direction;
        } else if (t > right) {
            result = p_lines[p_current_line].origin + right * p_lines[p_current_line].direction;
        } else {
            result = p_lines[p_current_line].origin + t * p_lines[p_current_line].direction;
        }
    }

    return true;
}

int ORCAAgent::run_linear_program_2(std::vector<DirectedLine> p_lines, glm::vec2 opt_velocity, bool optimize_direction, glm::vec2 &result) {
    if (optimize_direction) {
        result = opt_velocity * this->max_speed;
    } else if (glm::dot(opt_velocity, opt_velocity) > (this->max_speed * this->max_speed)) {
        result = glm::normalize(opt_velocity) * this->max_speed;
    } else {
        result = opt_velocity;
    }

    for (size_t i = 0; i < p_lines.size(); i++) {
        if (this->calculate_2d_determinant(p_lines[i].direction, p_lines[i].origin - result) > 0.0f) {
            // Result doesn't satisfy i line, compute new result
            glm::vec2 prev_result = result;

            if (!this->run_linear_program_1(p_lines, i, opt_velocity, optimize_direction, result)) {
                result = prev_result;
                return i;
            }
        }
    }
    return p_lines.size();
}

void ORCAAgent::run_linear_program_3(std::vector<DirectedLine> p_lines, int p_obstacle_lines_num, int p_begin_line, glm::vec2 &result) {
    float dist = 0.0f;

    for (size_t i = p_begin_line; i < p_lines.size(); i++) {
        if (this->calculate_2d_determinant(p_lines[i].direction, p_lines[i].origin - result) > dist) {
            std::vector<DirectedLine> obst_lines(p_lines.begin(), p_lines.begin() + p_obstacle_lines_num);

            for (size_t j = p_obstacle_lines_num; j < i; j++) {
                DirectedLine line;

                float det = this->calculate_2d_determinant(p_lines[i].direction, p_lines[j].direction);
                if (std::fabs(det) <= 0.00001f)  {
                    // Lines are parallel
                    if (glm::dot(p_lines[i].direction, p_lines[j].direction) > 0.0f) {
                        // Lines point in the same direction
                        continue;
                    } else {
                        // Lines point in opposite direction
                        line.origin = 0.5f * (p_lines[i].origin + p_lines[j].origin);
                    }
                } else {
                    line.origin = p_lines[i].origin + (this->calculate_2d_determinant(p_lines[j].direction, p_lines[i].origin - p_lines[j].origin) / det) * p_lines[i].direction;
                }

                line.direction = glm::normalize(p_lines[j].direction - p_lines[i].direction);
                obst_lines.push_back(line);
            }

            glm::vec2 prev_result = result;
            if (this->run_linear_program_2(obst_lines, glm::vec2(-p_lines[i].direction.y, p_lines[i].direction.x), true, result) < obst_lines.size()) {
                result = prev_result;
            }
            
            dist = this->calculate_2d_determinant(p_lines[i].direction, p_lines[i].origin - result);
        }
    }
}


float ORCAAgent::calculate_2d_determinant(glm::vec2 a, glm::vec2 b) {
    return a.x * b.y - a.y * b.x;
}

glm::vec2 ORCAAgent::get_position() {
    return this->position;
}

void ORCAAgent::set_position(glm::vec2 p_pos) {
    this->position = p_pos;
}

glm::vec2 ORCAAgent::get_target() {
    return this->target;
}

void ORCAAgent::set_target(glm::vec2 p_target) {
    this->target = p_target;
}

glm::vec2 ORCAAgent::get_velocity() {
    return this->velocity;
}

void ORCAAgent::set_velocity(glm::vec2 p_velocity) {
    this->velocity = p_velocity;
}

float ORCAAgent::get_radius() {
    return this->radius;
}

void ORCAAgent::set_radius(float p_rad) {
    this->radius = p_rad;
}

float ORCAAgent::get_neighbor_view_distance() {
    return this->neighbor_view_distance;
}

float ORCAAgent::get_time_horizon() {
    return this->time_horizon;
}
float ORCAAgent::get_max_speed() {
    return this->max_speed;
}

bool ORCAAgent::has_reached_target() {
    return glm::distance(this->position, this->target) < 1.0f;
}
