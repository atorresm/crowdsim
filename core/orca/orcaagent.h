#pragma once

#include "glm/glm.hpp"
#include "orcapointobstacle.h"
#include "directedline.h"
#include <vector>

class ORCAAgent {
    private:
        glm::vec2 position;
        glm::vec2 target;
        glm::vec2 velocity;
        glm::vec2 preferred_velocity;
        float radius;
        float neighbor_view_distance; // maximum distance to consider other agents neighbors
        float time_horizon; // amount of time for which computed velocity is safe
        float max_speed;
        float calculate_2d_determinant(glm::vec2 a, glm::vec2 b);
        bool run_linear_program_1(std::vector<DirectedLine> p_lines, int p_current_line, glm::vec2 opt_velocity, bool optimize_direction, glm::vec2 &result);
        int run_linear_program_2(std::vector<DirectedLine> p_lines, glm::vec2 opt_velocity, bool optimize_direction, glm::vec2 &result);
        void run_linear_program_3(std::vector<DirectedLine> p_lines, int p_obstacle_lines_num, int p_begin_line, glm::vec2 &result); // p_begin_line is where the linear program 2 failed
    public:
        ORCAAgent(glm::vec2 p_pos, glm::vec2 p_target);
        ~ORCAAgent();
        void update(std::vector<ORCAAgent*> p_neighbors, std::vector<ORCAPointObstacle*> p_nearby_obstacles, float p_delta);
        glm::vec2 get_position();
        void set_position(glm::vec2 p_pos);
        glm::vec2 get_target();
        void set_target(glm::vec2 p_target);
        glm::vec2 get_velocity();
        void set_velocity(glm::vec2 p_velocity);
        float get_radius();
        void set_radius(float p_rad);
        float get_neighbor_view_distance();
        float get_time_horizon();
        float get_max_speed();
        bool has_reached_target();
};