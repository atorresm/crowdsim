#include "orcasimulation.h"
#include "orcapointobstacle.h"
#include "../engine/flycamera.h"
#include "../engine/directionallight.h"
#include "../engine/gltfmodel.h"


ORCASimulation::ORCASimulation(std::vector<glm::vec2> agent_positions, std::vector<glm::vec2> agent_targets) {
    for (size_t i = 0; i < agent_positions.size(); i++) {
        ORCAAgent *a;
        if (i > agent_targets.size()) {
            // If no target is provided, don't move (use current position as target)
            a = new ORCAAgent(agent_positions[i], agent_positions[i]);
        } else {
            a = new ORCAAgent(agent_positions[i], agent_targets[i]);
        }
        this->agents.push_back(a);
    }

    // Initialize scene
    FlyCamera *cam = new FlyCamera();
    this->scn = new Scene(cam);
    Environment *env = new Environment(glm::vec3(1.0), glm::vec3(0.15));
    this->scn->set_environment(env);
    Shader vs("res/shaders/vertex.glsl");
    Shader fs("res/shaders/fragment.glsl");
    DirectionalLight *dl = new DirectionalLight(glm::vec3(1.0), glm::vec3(-0.2f, -1.0f, -0.3f));
    this->scn->add_light(dl);
    for (size_t i = 0; i < this->agents.size(); i++) {
        GLTFModel *gmodel = new GLTFModel("res/models/agent/ORCACilinder.gltf", vs, fs);
        glm::vec2 pos = this->agents[i]->get_position();
        gmodel->set_position(glm::vec3(pos.x, 0, pos.y));
        this->agent_models.push_back(gmodel);
        this->scn->add_item(gmodel);
    }
}

ORCASimulation::~ORCASimulation() {
    for (size_t i = 0; i < this->agents.size(); i++) {
        if (this->agents[i] != nullptr) {
            delete this->agents[i];
        }
        if (this->agent_models[i] != nullptr) {
            delete this->agent_models[i];
        }
    }
    for (size_t i = 0; i < this->obstacle_models.size(); i++) {
        if (this->obstacle_models[i] != nullptr) {
            delete this->obstacle_models[i];
        }
    }
    
}

void ORCASimulation::update(float p_delta) {
    for (size_t i = 0; i < this->agents.size(); i++) {
        ORCAAgent *a = this->agents[i];
        a->update(get_agent_neigbors(a), get_agent_nearby_obstacles(a), p_delta);
        glm::vec2 pos = a->get_position();
        this->agent_models[i]->set_position(glm::vec3(pos.x, 0, pos.y));
    }
}

void ORCASimulation::draw(float p_delta) {
    this->scn->draw(p_delta);
}

void ORCASimulation::add_obstacle(std::vector<glm::vec2> p_vertices) {
    if (p_vertices.size() != 4) {
        std::cout << "Obstacles must be a rectangle (4 verts)" << std::endl;
        return;
    }

    std::vector<float> model_verts;
    float mean_x = 0;
    float mean_y = 0;
    int first_obst_idx = this->obstacles.size();

    for (size_t i = 0; i < p_vertices.size(); i++) {
        ORCAPointObstacle *obst = new ORCAPointObstacle();
        obst->position = p_vertices[i];

        // Get previous and next points
        if (i != 0) {
            obst->previous = this->obstacles.back();
            obst->previous->next = obst;
        }
        if (i == p_vertices.size() - 1) {
            obst->next = this->obstacles[first_obst_idx];
            obst->next->previous = obst;
        }

        glm::vec2 next_point = p_vertices[i == p_vertices.size() - 1 ? 0 : i + 1];
        glm::vec2 prev_point = p_vertices[i == 0 ? p_vertices.size() - 1 : i - 1];

        // Direction to next point
        obst->direction = glm::normalize(next_point - p_vertices[i]);

        // Convexity
        // Is convex if next_point is at left of the |prev - current| segment,
        // this being if the determinant of the [prev - next; current - prev] is >= 0
        glm::vec2 a = prev_point - next_point;
        glm::vec2 b = p_vertices[i] - prev_point;
        float det = a.x * b.y - a.y * b.x;
        obst->is_convex = det >= 0.f;

        obst->index = this->obstacles.size();
        this->obstacles.push_back(obst);
        glm::vec2 pos = p_vertices[i];
        mean_x += pos.x;
        mean_y += pos.y;
        model_verts.push_back(pos.x);
        model_verts.push_back(0);
        model_verts.push_back(pos.y);
        // TODO better normals and uv
        model_verts.push_back(0.0);
        model_verts.push_back(1.0);
        model_verts.push_back(0.0);
        model_verts.push_back(1.0);
        model_verts.push_back(1.0);    
    }
    mean_x /= 4;
    mean_y /= 4;
    std::vector<unsigned int> indices = {
        0, 1, 3,
        1, 2, 3
    };
    Shader vs("res/shaders/vertex.glsl");
    Shader fs("res/shaders/fragment.glsl");
    Model *m = new Model(model_verts, indices, vs, fs);
    m->set_position(glm::vec3(mean_x, 0, mean_y));
    this->obstacle_models.push_back(m);
    this->scn->add_item(m);
}

float left_of(glm::vec2 a, glm::vec2 b, glm::vec2 point) {
    // >0 if point is left of segment a-b, <0 if not
    glm::vec2 first = a - point;
    glm::vec2 second = b - a;
    float det = first.x * second.y - first.y * second.x;
    return det;
}

std::vector<ORCAAgent*> ORCASimulation::get_agent_neigbors(ORCAAgent *p_agent) {
    // TODO: quadtree
    std::vector<ORCAAgent*> ret;
    for (size_t i = 0; i < this->agents.size(); i++) {
        if (this->agents[i] == p_agent) {
            continue;
        }
        if (glm::distance(p_agent->get_position(), this->agents[i]->get_position()) < p_agent->get_neighbor_view_distance()) {
            ret.push_back(this->agents[i]);
        }
    }
    return ret;
}

std::vector<ORCAPointObstacle*> ORCASimulation::get_agent_nearby_obstacles(ORCAAgent *p_agent) {
    // TODO: quadtree
    std::vector<ORCAPointObstacle*> ret;
    float view_dist = p_agent->get_time_horizon() * p_agent->get_max_speed() + p_agent->get_radius();
    for (size_t i = 0; i < this->obstacles.size(); i++) {
        ORCAPointObstacle *curr = this->obstacles[i];
        ORCAPointObstacle *next = curr->next;
        // Check if agent is at left obstacle segment
        float left = left_of(curr->position, next->position, p_agent->get_position());
        float dist = (left * left) / glm::dot(next->position - curr->position, next->position - curr->position);
        if (dist < view_dist && left < 0.0f) {
            ret.push_back(this->obstacles[i]);
        }
    }
    return ret;
}

bool ORCASimulation::is_finished() {
    for (size_t i = 0; i < this->agents.size(); i++) {
        if (!this->agents[i]->has_reached_target()) {
            return false;
        }
    }
    return true;
}
