#include "steersimulation.h"
#include "../../engine/engine.h"
#include <iostream>
#include <random>
#include <ctime>

SteerSimulation::SteerSimulation(std::vector<glm::vec2> p_agent_positions, std::vector<glm::vec2> p_agent_targets, float p_neigh_view_distance) {
    if (p_agent_targets.size() != p_agent_positions.size()) {
        std::cout << "ERROR: Targets list and position list don't have the same length" << std::endl;
    }
    for (size_t i = 0; i < p_agent_positions.size(); i++) {
        SteerAgent *a = new SteerAgent(i, p_agent_positions[i], p_agent_targets[i], p_neigh_view_distance, false);
        this->agents.push_back(a);
        Sprite *s = new Sprite("res/textures/flockagent.jpg", glm::vec2(a->get_radius() * 2));
        this->agent_models.push_back(s);
    }
}

SteerSimulation::SteerSimulation(int n_of_agents, int n_of_obstacles, float p_neigh_view_distance) {
    // Create agents with random starting positions
    std::default_random_engine gen(std::time(nullptr));
    // TODO: Use parametrized screen size (singleton?)
    std::uniform_int_distribution<int> dist_w(0, Engine::WINDOW_WIDTH);
    std::uniform_int_distribution<int> dist_h(0, Engine::WINDOW_HEIGHT);
    std::uniform_real_distribution<float> dist_tar(0, 500);
    for (size_t i = 0; i < n_of_agents; i++) {
        float x = (float) dist_w(gen);
        float y = (float) dist_h(gen);
        float xtar = dist_tar(gen);
        float ytar = dist_tar(gen);
        SteerAgent *agent = new SteerAgent(i, glm::vec2(x, y), glm::vec2(xtar, ytar), p_neigh_view_distance, false);
        Sprite *s = new Sprite("res/textures/flockagent.png", glm::vec2(agent->get_radius() * 2));
        this->agent_models.push_back(s);
        this->agents.push_back(agent);
    }
    for (size_t i = 0; i < n_of_obstacles; i++) {
        float x = (float) dist_w(gen);
        float y = (float) dist_h(gen);
        SteerAgent *agent = new SteerAgent(i, glm::vec2(x, y), glm::vec2(-1), 0.f, true, 15);
        Sprite *s = new Sprite("res/textures/obstacle.png", glm::vec2(agent->get_radius() * 2));
        this->agent_models.push_back(s);
        this->agents.push_back(agent);
    }
    
}

SteerSimulation::~SteerSimulation() {
    for (size_t i = 0; i < this->agents.size(); i++) {
        if (this->agents[i] != nullptr) {
            delete this->agents[i];
        }
        if (this->agent_models[i] != nullptr) {
            delete this->agent_models[i];
        }
    }
}

void SteerSimulation::update(float p_delta) {
    for (size_t i = 0; i < this->agents.size(); i++) {
        SteerAgent *a = this->agents[i];
        a->update(this->get_neighbors(a));
    }
}

void SteerSimulation::draw(float p_delta) {
    for (size_t i = 0; i < this->agent_models.size(); i++) {
        this->agent_models[i]->set_position(this->agents[i]->get_position());
        this->agent_models[i]->draw(p_delta);
    }
}

void SteerSimulation::add_circular_obstacle(glm::vec2 p_pos, float p_radius) {
    SteerAgent *a = new SteerAgent(this->agents.size() + 1, p_pos, glm::vec2(-1), 0.0f, true, p_radius);
    Sprite *s = new Sprite("res/textures/obstacle.jpg", glm::vec2(p_radius * 2));
    s->set_position(p_pos);
    this->agent_models.push_back(s);
    this->agents.push_back(a);
}

std::vector<SteerAgent*> SteerSimulation::get_neighbors(SteerAgent* p_agent) {
    // TODO: quadtree
    // TODO: check for obstacles
    std::vector<SteerAgent*> ret;
    for (size_t i = 0; i < this->agents.size(); i++) {
        SteerAgent *a = this->agents[i];
        float dist = glm::distance(a->get_position(), p_agent->get_position()) - a->get_radius() - p_agent->get_radius();
        if (a != p_agent &&  dist < p_agent->get_neighbor_view_distance()) {
            ret.push_back(a);
        }
    }
    return ret;
}