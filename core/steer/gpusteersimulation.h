#pragma once

#include "../../engine/sprite.h"
#include "../../engine/model.h"
#include "../../engine/scene.h"
#include "../../engine/material.h"
#include <vector>

class GPUSteerSimulation {
    private:
        // 2D / 3D visualization
        std::vector<Sprite*> agent_models;
        std::vector<Sprite*> obstacle_models;
        std::vector<Model*> agent_models_3d;
        std::vector<Model*> obstacle_models_3d;
        Material *agent_material;
        Material *obstacle_material;
        Scene *scn_3d;
        bool show_in_3d;
        float divider_3d; // factor of scale from pixels to meters

        // Simulation params
        float max_speed = 3.5f;
        float max_force = 1.0f;
        float view_distance = 10.0f;
        int n_agents = 0;
        int n_obstacles = 0;

        // Arrays for processing
        float *vel_x;
        float *vel_y;
        float *pos_x;
        float *pos_y;
        float *target_x;
        float *target_y;
        float *radius;

        std::vector<float> obstacle_pos_x;
        std::vector<float> obstacle_pos_y;
        std::vector<float> obstacle_radius;

        float *dev_vel_x;
        float *dev_vel_y;
        float *dev_pos_x;
        float *dev_pos_y;
        float *dev_target_x;
        float *dev_target_y;
        float *dev_radius;

        float *dev_obstacle_pos_x = nullptr;
        float *dev_obstacle_pos_y = nullptr;
        float *dev_obstacle_radius = nullptr;

        float *dev_out_vel_x;
        float *dev_out_vel_y;
        float *dev_out_pos_x;
        float *dev_out_pos_y;
    public:
        GPUSteerSimulation(std::vector<glm::vec2> p_agent_positions, 
                           std::vector<glm::vec2> p_agent_targets,
                           float p_neigh_view_distance,
                           bool p_show_in_3d = false);
        ~GPUSteerSimulation();
        void update(float p_delta);
        void draw(float p_delta);
        void add_circular_obstacle(glm::vec2 p_pos, float p_radius);
};