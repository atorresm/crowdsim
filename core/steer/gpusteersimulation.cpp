#include "gpusteersimulation.h"
#include "vuda/vuda.hpp"
#include "../../engine/flycamera.h"
#include "../../engine/directionallight.h"
#include "../../engine/gltfmodel.h"
#include <cmath>

GPUSteerSimulation::GPUSteerSimulation(std::vector<glm::vec2> p_agent_positions, std::vector<glm::vec2> p_agent_targets, float p_neigh_view_distance, bool p_show_in_3d) {
    if (p_agent_positions.size() != p_agent_targets.size()) {
        std::cout << "ERROR: positions and targets list must be of the same size" << "\n";
    }
    this->view_distance = p_neigh_view_distance;
    this->show_in_3d = p_show_in_3d;

    vuda::setDevice(0);

    const int n = p_agent_positions.size();
    this->n_agents = n;

    // Initialize arays
    this->vel_x = new float[n];
    this->vel_y = new float[n];
    this->pos_x = new float[n];
    this->pos_y = new float[n];
    this->target_x = new float[n];
    this->target_y = new float[n];
    this->radius = new float[n];

    vuda::malloc((void**) &this->dev_vel_x, n * sizeof(float));
    vuda::malloc((void**) &this->dev_vel_y, n * sizeof(float));
    vuda::malloc((void**) &this->dev_pos_x, n * sizeof(float));
    vuda::malloc((void**) &this->dev_pos_y, n * sizeof(float));
    vuda::malloc((void**) &this->dev_target_x, n * sizeof(float));
    vuda::malloc((void**) &this->dev_target_y, n * sizeof(float));
    vuda::malloc((void**) &this->dev_radius, n * sizeof(float));

    vuda::malloc((void**) &this->dev_out_vel_x, n * sizeof(float));
    vuda::malloc((void**) &this->dev_out_vel_y, n * sizeof(float));
    vuda::malloc((void**) &this->dev_out_pos_x, n * sizeof(float));
    vuda::malloc((void**) &this->dev_out_pos_y, n * sizeof(float));

    // Dummy malloc for obstacle data
    vuda::malloc((void**) &this->dev_obstacle_pos_x, 1);
    vuda::malloc((void**) &this->dev_obstacle_pos_y, 1);
    vuda::malloc((void**) &this->dev_obstacle_radius, 1);

    if (this->show_in_3d) {
        // Setup scene
        FlyCamera *cam = new FlyCamera();
        this->scn_3d = new Scene(cam);
        Environment *env = new Environment(glm::vec3(1.0), glm::vec3(0.15));
        this->scn_3d->set_environment(env);
        unsigned char agent_tex_data[4] = {200, 200, 200, 255};
        unsigned char obstacle_tex_data[4] = {200, 20, 20, 255};
        this->agent_material = new Material(new Texture(agent_tex_data, 1, 1, 4), new Texture(agent_tex_data, 1, 1, 4), 1.0);
        this->obstacle_material = new Material(new Texture(obstacle_tex_data, 1, 1, 4), new Texture(obstacle_tex_data, 1, 1, 4), 1.0);
        DirectionalLight *dl = new DirectionalLight(glm::vec3(1.0), glm::vec3(-0.2f, -1.0f, -0.3f));
        this->scn_3d->add_light(dl);
    }

    this->divider_3d = this->show_in_3d ? 10.0f : 1.0f;
    if (this->show_in_3d) {
        this->view_distance /= divider_3d;
        this->max_force /= divider_3d;
        this->max_speed /= divider_3d;
    }

    for (size_t i = 0; i < n; i++) {
        glm::vec2 pos = p_agent_positions[i];
        glm::vec2 tar = p_agent_targets[i];
        this->vel_x[i] = 0.0f;
        this->vel_y[i] = 0.0f;
        this->pos_x[i] = pos.x / divider_3d;
        this->pos_y[i] = pos.y / divider_3d;
        this->target_x[i] = tar.x / divider_3d;
        this->target_y[i] = tar.y / divider_3d;
        this->radius[i] = 2.5f / divider_3d;
        if (this->show_in_3d) {
            Shader vs("res/shaders/vertex.glsl");
            Shader fs("res/shaders/fragment.glsl");
            GLTFModel *s = new GLTFModel("res/models/agent/ORCACilinder.gltf", vs, fs);
            s->set_material(this->agent_material);
            this->agent_models_3d.push_back(s);
            this->scn_3d->add_item(s);
        } else {
            this->agent_models.push_back(new Sprite("res/textures/flockagent.jpg", glm::vec2(2.5f * 2.0f)));
        }
    }

    // Move these arrays to GPU now since they won't change during the simulation
    vuda::memcpy(this->dev_target_x, this->target_x, n * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_target_y, this->target_y, n * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_radius, this->radius, n * sizeof(float), vuda::memcpyHostToDevice);

    Shader::compile_glsl_file_to_spv_file(Shader::ShaderType::COMPUTE, "res/shaders/steer.glsl", "res/shaders/steer.spv", true);
}

GPUSteerSimulation::~GPUSteerSimulation() {
    for (size_t i = 0; i < this->agent_models.size(); i++) {
        if (this->agent_models[i] != nullptr) {
            delete this->agent_models[i];
        }
    }
    for (size_t i = 0; i < this->obstacle_models.size(); i++) {
        if (this->obstacle_models[i] != nullptr) {
            delete this->obstacle_models[i];
        }
    }
}

void GPUSteerSimulation::update(float p_delta) {
    // Compute params
    const int stream_id = 0;
    const int blocks = std::ceil(n_agents / 128.f);
    const int threads = 128;

    vuda::memcpy(this->dev_vel_x, this->vel_x, n_agents * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_vel_y, this->vel_y, n_agents * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_pos_x, this->pos_x, n_agents * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_pos_y, this->pos_y, n_agents * sizeof(float), vuda::memcpyHostToDevice);

    vuda::launchKernel("res/shaders/steer.spv", "main", stream_id, blocks, threads,
        this->dev_vel_x,
        this->dev_vel_y,
        this->dev_pos_x,
        this->dev_pos_y,
        this->dev_target_x,
        this->dev_target_y,
        this->dev_radius,
        this->dev_obstacle_pos_x,
        this->dev_obstacle_pos_y,
        this->dev_obstacle_radius,
        this->dev_out_vel_x,
        this->dev_out_vel_y,
        this->dev_out_pos_x,
        this->dev_out_pos_y,
        this->n_agents,
        this->n_obstacles,
        this->view_distance,
        this->max_speed,
        this->max_force
    );

    vuda::memcpy(this->vel_x, this->dev_out_vel_x, n_agents * sizeof(float), vuda::memcpyDeviceToHost);
    vuda::memcpy(this->vel_y, this->dev_out_vel_y, n_agents * sizeof(float), vuda::memcpyDeviceToHost);
    vuda::memcpy(this->pos_x, this->dev_out_pos_x, n_agents * sizeof(float), vuda::memcpyDeviceToHost);
    vuda::memcpy(this->pos_y, this->dev_out_pos_y, n_agents * sizeof(float), vuda::memcpyDeviceToHost);
}

void GPUSteerSimulation::draw(float p_delta) {
    if (this->show_in_3d) {
        for (size_t i = 0; i < this->agent_models_3d.size(); i++) {
            glm::vec3 pos = glm::vec3(this->pos_x[i], 0, this->pos_y[i]);
            this->agent_models_3d[i]->set_position(pos);
        }
        this->scn_3d->draw(p_delta);
    } else {
        for (size_t i = 0; i < this->agent_models.size(); i++) {
            this->agent_models[i]->set_position(glm::vec2(this->pos_x[i], this->pos_y[i]));
            this->agent_models[i]->draw(p_delta);
        }
        for (size_t i = 0; i < this->obstacle_models.size(); i++) {
            this->obstacle_models[i]->draw(p_delta);
        }
    }
}

void GPUSteerSimulation::add_circular_obstacle(glm::vec2 p_pos, float p_radius) {
    // TODO: more efficient memory handling
    glm::vec2 pos = glm::vec2(p_pos.x / this->divider_3d, p_pos.y / this->divider_3d);
    this->obstacle_pos_x.push_back(pos.x);
    this->obstacle_pos_y.push_back(pos.y);
    this->obstacle_radius.push_back(p_radius / this->divider_3d);
    this->n_obstacles++;
    if (this->dev_obstacle_pos_x != nullptr) {
        // We have already added obstacles
        vuda::free(this->dev_obstacle_pos_x);
        vuda::free(this->dev_obstacle_pos_y);
        vuda::free(this->dev_obstacle_radius);
        this->dev_obstacle_pos_x = nullptr;
        this->dev_obstacle_pos_y = nullptr;
        this->dev_obstacle_radius = nullptr;
    }
    const int n = this->obstacle_pos_x.size();
    vuda::malloc((void**) &this->dev_obstacle_pos_x, n * sizeof(float));
    vuda::malloc((void**) &this->dev_obstacle_pos_y, n * sizeof(float));
    vuda::malloc((void**) &this->dev_obstacle_radius, n * sizeof(float));
    vuda::memcpy(this->dev_obstacle_pos_x, this->obstacle_pos_x.data(), n * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_obstacle_pos_y, this->obstacle_pos_y.data(), n * sizeof(float), vuda::memcpyHostToDevice);
    vuda::memcpy(this->dev_obstacle_radius, this->obstacle_radius.data(), n * sizeof(float), vuda::memcpyHostToDevice);
    if (this->show_in_3d) {
        Shader vs("res/shaders/vertex.glsl");
        Shader fs("res/shaders/fragment.glsl");
        GLTFModel *s = new GLTFModel("res/models/agent/ORCACilinder.gltf", vs, fs);
        s->set_material(this->obstacle_material);
        s->set_position(glm::vec3(pos.x, 0, pos.y));
        this->obstacle_models_3d.push_back(s);
        this->scn_3d->add_item(s);
    } else {
        Sprite *s = new Sprite("res/textures/obstacle.png", glm::vec2(p_radius * 2.0f));
        s->set_position(pos);
        this->obstacle_models.push_back(s);
    }
}

