#pragma once

#include <vector>
#include "glm/glm.hpp"

class SteerAgent {
    private:
        const float MAX_SPEED = 3.5;
        const float MAX_FORCE = 1.0;
        int id;
        float neighbor_view_distance;
        bool is_obstacle;
        float radius;
        glm::vec2 position;
        glm::vec2 velocity;
        glm::vec2 acceleration;
        glm::vec2 target;
        void clamp_position();
        glm::vec2 calc_alignment(const std::vector<SteerAgent*> &p_neighbours);
        glm::vec2 calc_separation(const std::vector<SteerAgent*> &p_neighbours);
        glm::vec2 calc_cohesion(const std::vector<SteerAgent*> &p_neighbours);
        glm::vec2 calc_seek();
        glm::vec2 limit_vector(const glm::vec2 &p_vec, float limit);
    public:
        SteerAgent(int p_id, glm::vec2 p_pos, glm::vec2 p_target, float p_neighbor_view_distance, bool p_is_obstacle, float p_radius = 2.5f);
        ~SteerAgent();
        glm::vec2 get_position() const;
        void set_position(const glm::vec2 &p_pos);
        glm::vec2 get_velocity() const;
        void set_velocity(const glm::vec2 &p_vel);
        glm::vec2 get_acceleration() const;
        void set_acceleration(const glm::vec2 &p_acc);
        float get_neighbor_view_distance() const;
        float get_radius() const;
        bool is_obstacle_agent() const;
        int get_id() const;
        void update(const std::vector<SteerAgent*> &p_neighbours);
        inline bool operator==(const SteerAgent &b) { return this->id == b.get_id(); };
        inline bool operator!=(const SteerAgent &b) { return this->id != b.get_id(); };
};