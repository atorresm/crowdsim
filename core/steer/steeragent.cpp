#include "steeragent.h"
#include "../../engine/engine.h"

SteerAgent::SteerAgent(int p_id, glm::vec2 p_pos, glm::vec2 p_target, float p_neighbor_view_distance, bool p_is_obstacle, float p_radius) {
    this->id = p_id;
    this->position = p_pos;
    this->velocity = glm::vec2(0);
    this->target = p_target;
    this->neighbor_view_distance = p_neighbor_view_distance;
    this->radius = p_radius;
    this->is_obstacle = p_is_obstacle;
    this->acceleration = glm::vec2(0.0);
}

SteerAgent::~SteerAgent() {

}

void SteerAgent::clamp_position() {
    int w = Engine::WINDOW_WIDTH;
    int h = Engine::WINDOW_HEIGHT;
    int sprite_x = this->radius * 2.f;
    int sprite_y = this->radius * 2.f;
    //if (this->position.x > (w + sprite_x)) {
    //    this->position.x = -sprite_x; 
    //} else if (this->position.x < -sprite_x) {
    //    this->position.x = w + sprite_x;
    //}
    if (this->position.y > (h + sprite_y)) {
        this->position.y = -sprite_y;
    } else if (this->position.y < -sprite_y) {
        this->position.y = h + sprite_y;
    }
}

glm::vec2 SteerAgent::limit_vector(const glm::vec2 &p_vec, float limit) {
    glm::vec2 ret = glm::vec2(p_vec);
    float square_limit = limit * limit;
    float squared_magnitude = ret.x * ret.x + ret.y * ret.y;
    if (squared_magnitude > square_limit) {
        ret = glm::normalize(ret) * limit;
    }
    return ret;
}

glm::vec2 SteerAgent::calc_alignment(const std::vector<SteerAgent*> &p_neighbours) {
    if (p_neighbours.size() == 0) {
        return glm::vec2(0.0);
    }

    // Calc mean velocity
    glm::vec2 steering_force = glm::vec2(0.0);
    for (size_t i = 0; i < p_neighbours.size(); i++) {
        steering_force += p_neighbours[i]->get_velocity();
    }
    steering_force /= p_neighbours.size();
    steering_force = glm::normalize(steering_force);
    steering_force *= this->MAX_SPEED; // Maintain full speed
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    
    return steering_force;
}

glm::vec2 SteerAgent::calc_separation(const std::vector<SteerAgent*> &p_neighbours) {
    if (p_neighbours.size() == 0) {
        return glm::vec2(0.0);
    }

    glm::vec2 steering_force = glm::vec2(0.0);
    for (size_t i = 0; i < p_neighbours.size(); i++) {
        float d = glm::distance(this->position, p_neighbours[i]->get_position());
        glm::vec2 difference = this->position - p_neighbours[i]->get_position();
        difference /= d * d;
        steering_force += difference;
    }
    steering_force /= p_neighbours.size();
    steering_force = glm::normalize(steering_force);
    steering_force *= this->MAX_SPEED; // Maintain full speed
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    
    return steering_force;
}

glm::vec2 SteerAgent::calc_cohesion(const std::vector<SteerAgent*> &p_neighbours) {
    if (p_neighbours.size() == 0) {
        return glm::vec2(0.0);
    }
    
    // Calc mean position
    glm::vec2 steering_force = glm::vec2(0.0);
    for (size_t i = 0; i < p_neighbours.size(); i++) {
        steering_force += p_neighbours[i]->get_position();
    }
    steering_force /= p_neighbours.size();
    steering_force -= this->position;
    steering_force = glm::normalize(steering_force);
    steering_force *= this->MAX_SPEED; // Maintain full speed
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    
    return steering_force;
}

glm::vec2 SteerAgent::calc_seek() {
    glm::vec2 steering_force = glm::vec2(0.0);
    steering_force = glm::normalize(this->target - this->position) * this->MAX_SPEED;
    steering_force -= this->velocity;
    steering_force = this->limit_vector(steering_force, this->MAX_FORCE);
    return steering_force;
}

glm::vec2 SteerAgent::get_position() const {
    return this->position;
}

void SteerAgent::set_position(const glm::vec2 &p_pos) {
    this->position = p_pos;
}

glm::vec2 SteerAgent::get_velocity() const {
    return this->velocity;
}

void SteerAgent::set_velocity(const glm::vec2 &p_vel) {
    this->velocity = p_vel;
}

glm::vec2 SteerAgent::get_acceleration() const {
    return this->acceleration;
}

void SteerAgent::set_acceleration(const glm::vec2 &p_acc) {
    this->acceleration = p_acc;
}

int SteerAgent::get_id() const {
    return this->id;
}

float SteerAgent::get_radius() const {
    return this->radius;
}

bool SteerAgent::is_obstacle_agent() const {
    return this->is_obstacle;
}

float SteerAgent::get_neighbor_view_distance() const {
    return this->neighbor_view_distance;
}

void SteerAgent::update(const std::vector<SteerAgent*> &p_neighbours) {
    if (this->is_obstacle) {
        return;
    }

    this->clamp_position();

    //glm::vec2 alignment = this->calc_alignment(p_neighbours);
    glm::vec2 separation = this->calc_separation(p_neighbours);
    //glm::vec2 cohesion = this->calc_cohesion(p_neighbours);
    glm::vec2 seek = this->calc_seek();
    this->acceleration += 0.5f * separation + 0.15f * seek;

    this->position += this->velocity;
    this->velocity += this->acceleration;
    this->velocity = this->limit_vector(this->velocity, this->MAX_SPEED);
    this->acceleration = glm::vec2(0.0); // reset acceleration
}
