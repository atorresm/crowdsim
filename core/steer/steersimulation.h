#pragma once

#include "steeragent.h"
#include "../../engine/sprite.h"
#include <vector>

class SteerSimulation {
    private:
        std::vector<SteerAgent*> agents;
        std::vector<Sprite*> agent_models;
    public:
        SteerSimulation(std::vector<glm::vec2> p_agent_positions, 
                        std::vector<glm::vec2> p_agent_targets, 
                        float p_neigh_view_distance);
        SteerSimulation(int n_of_agents, int n_of_obstacles, float p_neigh_view_distance);
        ~SteerSimulation();
        void update(float p_delta);
        void draw(float p_delta);
        void add_circular_obstacle(glm::vec2 p_pos, float p_radius);
        std::vector<SteerAgent*> get_neighbors(SteerAgent* p_agent);
};