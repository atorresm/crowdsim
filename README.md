# crowdsim

Crowd simulation using GPU parallel computing, developed as a final degree project at the University of Sevilla.

This simulator implements the following models:

* Flocking
* ORCA
* Obstacle avoidance with targets using steering behaviors

With these simulators comes a complete graphic engine built on top of OpenGL.
For the GPU acceleration, the library VUDA (https://github.com/jgbit/vuda) built on top of Vulkan is used,
so this project must be run on a Vulkan-capable device if you want to run the GPU-accelerated 
versions of the crowd simulation systems.

## Dependencies

The following dependencies are needed:

* CMake 3.10 or later
* shaderc (https://github.com/google/shaderc)
* Vulkan development libs
* GLFW (https://www.glfw.org/)

## Compiling and running

Having the dependencies installed, you can compile the project using:


    cmake . && make

On the root directory. After this, you will have a binary created at bin/crowdsim.
Run this binary from the root directory like this:

    ./bin/crowdsim

So the project uses the root directory as the working directory.

## Settings

Under the `core` directory you can find the different implementations of the simulation systems.
You can adjust them using the parameters on their constructors. Then, run it on the main loop 
defined on the `main.cpp` file. 

## Integration

You can also use the included graphic engine as a standalone tool. Just add the `engine` 
folder to your project and use the engine.

You can also add new simulations to use with the included graphic engine.
